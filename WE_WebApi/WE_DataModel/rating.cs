﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WE_DataModel
{
    [Table("rating")]
    public partial class rating
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public int Value { get; set; }
        public virtual router Router { get; set; }
        public virtual user User { get; set; }
        public DateTime Time { get; set; }

    }
}
