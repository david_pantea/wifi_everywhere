namespace WE_DataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using WE_DataModel.User;

    public class WeModel : DbContext
    {
        public WeModel()
            : base("name=WeModel")
        {
            Database.SetInitializer<WeModel>(new CreateDatabaseIfNotExists<WeModel>());
           // Database.SetInitializer(new WeModelInitializer());
        }

        public virtual DbSet<user> Users { get; set; }
        public virtual DbSet<location> Locations { get; set; }
        public virtual DbSet<router> Routers { get; set; }
        public virtual DbSet<role> Roles { get; set; }
        public virtual DbSet<rating> Rating { get; set; }
        public virtual DbSet<point> Point { get; set; }
        public virtual DbSet<unlock> Unlocks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<user>()
                .Property(e => e.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<point>()
                .Property(e => e.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);


            //location
            // modelBuilder.Entity<location>().Property(p => p.Latitude).HasPrecision(18, 16);
            //  modelBuilder.Entity<location>().Property(p => p.Longitude).HasPrecision(18, 16);
        }
    }
}
