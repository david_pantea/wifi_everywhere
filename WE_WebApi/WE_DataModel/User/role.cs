﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WE_DataModel.User
{

    [Table("role")]
    public partial class role
    {

        public role()
        {
            this.User = new HashSet<user>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(30)]
        public string Name { get; set; }
        [StringLength(30)]
        public string Description { get; set; }
        public virtual ICollection<user> User { get; set; }
    }
}
