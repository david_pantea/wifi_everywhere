namespace WE_DataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using WE_DataModel.User;

    [Table("user")]
    public partial class user
    {
        public user()
        {
            this.Roles = new HashSet<role>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        public bool IsLocked { get; set; }
        [StringLength(50)]
        [Index(IsUnique = true)]
        public string Username { get; set; }
        public string Email { get; set; }
        [StringLength(32)]
        public string Password { get; set; }
       // public int Role_ID { get; set; }
        public virtual ICollection<role>Roles { get; set; }
        public string InvitedById { get; set; }
        public string InvitationCode { get; set; }
        public virtual point Points { get; set; }

    }
}
