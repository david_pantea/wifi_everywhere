﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel.User;

namespace WE_DataModel
{
    public class WeModelInitializer : DropCreateDatabaseAlways<WeModel>
    {
        protected override void Seed(WeModel context)
        {
            var adminRole = new role() { ID = 1, Name = "Administrator", Description = "Admin" };
            var userRole = new role() { ID = 2, Name = "User", Description = "Regular user" };

            //========//

            var adminUser = new user() { FirstName = "David", LastName = "Pantea", Username = "david", IsLocked = false, Password = "172522ec1028ab781d9dfd17eaca4427", Roles = new List<role>() };

            adminUser.Roles.Add(adminRole);


            context.Users.Add(adminUser);

            base.Seed(context);
        }
    }
}
