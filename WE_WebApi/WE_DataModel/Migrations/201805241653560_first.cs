namespace WE_DataModel.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class first : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.location",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Location = c.Geography(),
                        City = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.router",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 100),
                        IsDeleted = c.Boolean(nullable: false),
                        Password = c.String(),
                        Location_ID = c.Int(),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.location", t => t.Location_ID)
                .ForeignKey("dbo.user", t => t.User_ID)
                .Index(t => t.Location_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.user",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        IsLocked = c.Boolean(nullable: false),
                        Username = c.String(maxLength: 50),
                        Email = c.String(),
                        Password = c.String(maxLength: 32),
                        InvitedById = c.String(),
                        InvitationCode = c.String(),
                        Points_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.point", t => t.Points_ID)
                .Index(t => t.Username, unique: true)
                .Index(t => t.Points_ID);
            
            CreateTable(
                "dbo.point",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.role",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 30),
                        Description = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.rating",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Time = c.DateTime(nullable: false),
                        Router_ID = c.Int(),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.router", t => t.Router_ID)
                .ForeignKey("dbo.user", t => t.User_ID)
                .Index(t => t.Router_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.unlock",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Router_ID = c.Int(),
                        User_ID = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.router", t => t.Router_ID)
                .ForeignKey("dbo.user", t => t.User_ID)
                .Index(t => t.Router_ID)
                .Index(t => t.User_ID);
            
            CreateTable(
                "dbo.roleusers",
                c => new
                    {
                        role_ID = c.Int(nullable: false),
                        user_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.role_ID, t.user_ID })
                .ForeignKey("dbo.role", t => t.role_ID, cascadeDelete: true)
                .ForeignKey("dbo.user", t => t.user_ID, cascadeDelete: true)
                .Index(t => t.role_ID)
                .Index(t => t.user_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.unlock", "User_ID", "dbo.user");
            DropForeignKey("dbo.unlock", "Router_ID", "dbo.router");
            DropForeignKey("dbo.rating", "User_ID", "dbo.user");
            DropForeignKey("dbo.rating", "Router_ID", "dbo.router");
            DropForeignKey("dbo.router", "User_ID", "dbo.user");
            DropForeignKey("dbo.roleusers", "user_ID", "dbo.user");
            DropForeignKey("dbo.roleusers", "role_ID", "dbo.role");
            DropForeignKey("dbo.user", "Points_ID", "dbo.point");
            DropForeignKey("dbo.router", "Location_ID", "dbo.location");
            DropIndex("dbo.roleusers", new[] { "user_ID" });
            DropIndex("dbo.roleusers", new[] { "role_ID" });
            DropIndex("dbo.unlock", new[] { "User_ID" });
            DropIndex("dbo.unlock", new[] { "Router_ID" });
            DropIndex("dbo.rating", new[] { "User_ID" });
            DropIndex("dbo.rating", new[] { "Router_ID" });
            DropIndex("dbo.user", new[] { "Points_ID" });
            DropIndex("dbo.user", new[] { "Username" });
            DropIndex("dbo.router", new[] { "User_ID" });
            DropIndex("dbo.router", new[] { "Location_ID" });
            DropTable("dbo.roleusers");
            DropTable("dbo.unlock");
            DropTable("dbo.rating");
            DropTable("dbo.role");
            DropTable("dbo.point");
            DropTable("dbo.user");
            DropTable("dbo.router");
            DropTable("dbo.location");
        }
    }
}
