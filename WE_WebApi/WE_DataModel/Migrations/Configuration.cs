namespace WE_DataModel.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using WE_DataModel.User;

    internal sealed class Configuration : DbMigrationsConfiguration<WE_DataModel.WeModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(WE_DataModel.WeModel context)
        {
            var adminRole = new role { ID = 1, Name = "Administrator", Description = "Admin" };
            var userRole = new role { ID = 2, Name = "User", Description = "Regular user" };


            //========//

            var adminUser = new user { FirstName = "David", LastName = "Pantea", Username = "david", IsLocked = false, Password = "172522ec1028ab781d9dfd17eaca4427", Roles = new List<role>(), Email = "x", InvitationCode = "123123", Points = new point { Amount = 10 } };
            var simpleUser = new user { FirstName = "David", LastName = "Pantea", Username = "user", IsLocked = false, Password = "172522ec1028ab781d9dfd17eaca4427", Roles = new List<role>(), Email = "Y", InvitationCode = "123124", Points = new point { Amount = 10 } };


            adminUser.Roles.Add(adminRole);
            adminUser.Roles.Add(userRole);

            context.Users.Add(adminUser);

        }
    }
}
