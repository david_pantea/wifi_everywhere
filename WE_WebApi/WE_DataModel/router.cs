﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WE_DataModel
{
    [Table("router")]
    public partial class router
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public bool IsDeleted { get; set; }
        public string Password { get; set; }
        public virtual location Location { get; set; }
        public virtual user User { get; set; }

    }


}
