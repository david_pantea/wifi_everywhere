﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WE_DataModel
{
    [Table("location")]
    public partial class location
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        public DbGeography Location { get; set; }
        public string City { get; set; }
        public virtual ICollection<router> Routers { get; set; }
    }
}
