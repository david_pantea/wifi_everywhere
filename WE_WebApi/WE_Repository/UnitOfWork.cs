﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel;
using WE_DataModel.User;

namespace WE_Repository
{
    public class UnitOfWork : IDisposable
    {
        private WeModel context = new WeModel();
        private GenericRepository<user> userRepository; 
        private GenericRepository<router> routerRepository;
        private GenericRepository<location> locationRepository;
        private GenericRepository<rating> ratingRepository;
        private GenericRepository<role> roleRepository;
        private GenericRepository<unlock> unlockRepository;

        public GenericRepository<user> UserRepository
        {
            get
            {
                return this.userRepository ?? new GenericRepository<user>(context);
            }
        }

        public GenericRepository<router> RouterRepository
        {
            get
            {
                return this.routerRepository ?? new GenericRepository<router>(context);
            }
        }

        public GenericRepository<location> LocationRepository
        {
            get
            {
                return this.locationRepository ?? new GenericRepository<location>(context);
            }
        }

        public GenericRepository<rating> RatingRepository
        {
            get
            {
                return this.ratingRepository ?? new GenericRepository<rating>(context);
            }
        }

        public GenericRepository<role> RoleRepository
        {
            get
            {
                return this.roleRepository ?? new GenericRepository<role>(context);
            }
        }

        public GenericRepository<unlock> UnlockRepository
        {
            get
            {
                return this.unlockRepository ?? new GenericRepository<unlock>(context);
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}

