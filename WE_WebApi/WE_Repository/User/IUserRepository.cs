﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel;

namespace WE_Repository
{
    public interface IUserRepository : IDisposable
    {
        IEnumerable<user> GetUsers();
        user GetUserByID(int userID);
        void InsertUser(user user);
        void DeleteUser(int userID);
        void UpdateUser(user user);
        void Save();
    }
}
