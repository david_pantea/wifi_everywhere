﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel;

namespace WE_Repository
{
    public class UserRepository: IUserRepository, IDisposable
    {
        private WeModel context;
        public UserRepository(WeModel context)
        {
            this.context = context;
        }

        public void DeleteUser(int userID)
        {
            user user = context.Users.Find(userID);
            context.Users.Remove(user);
        }

        public user GetUserByID(int userID)
        {
            return context.Users.Find(userID);
        }

        public IEnumerable<user> GetUsers()
        {
            return context.Users.ToList();
        }

        public void InsertUser(user user)
        {
            context.Users.Add(user);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void UpdateUser(user user)
        {
            context.Entry(user).State = EntityState.Modified;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UserRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
