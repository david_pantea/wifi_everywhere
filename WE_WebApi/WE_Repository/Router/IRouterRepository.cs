﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel;

namespace WE_Repository.Router
{
    public interface IRouterRepository
    {
        IEnumerable<router> Get();
        router GetUserByID(int routerID);
        void Insert(router router);
        void Delete(int routerID);
        void Update(router router);
        void Save();
    }
}
