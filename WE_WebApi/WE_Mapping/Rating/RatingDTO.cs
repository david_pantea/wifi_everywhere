﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WE_Mapping
{
    public class RatingDTO
    {
        public int ID { get; set; }
        public int Value { get; set; }
        public int RouterID { get; set; }
        public int UserID { get; set; }
        public DateTime Time { get; set; }
    }
}
