﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel;
using WE_Mapping.Location;
using WE_Repository;

namespace WE_Mapping.Router
{

    public class RouterDTO
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        public int ID { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string City { get; set; }
        public string AddedBy { get; set; }
        public int LocationID { get; set; }
        public LocationDTO Location { get; set; }

        public double Rating
        {
            get
            {
                var list = unitOfWork.RatingRepository.Get(x => x.Router.ID == ID).ToList();
                var sum = list.Sum(val => val.Value);
                if (list.Count() != 0)
                    return sum / list.Count();
                return 0;
            }
        }
    }
}

