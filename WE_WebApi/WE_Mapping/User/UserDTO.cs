﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WE_DataModel;
using WE_DataModel.User;
using WE_Repository;

namespace WE_Mapping.User
{
    public class UserDTO
    {
       // UnitOfWork unitOfWork = new UnitOfWork();
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        //public ICollection<role> Roles { get; set; }
        public string Token { get; set; }
        public string InvitationCode { get; set; }
        public string InvitedById { get; set; }
        public string Password { get; set; }
        public ICollection<RoleDTO> Roles { get; set; }
        public point Points { get; set; }
        //public user InvitedBy
        //{
        //    get
        //    {
        //        return unitOfWork.UserRepository.Get(x => x.InvitationCode == InvitedById).FirstOrDefault();
        //    }
        //}
    }
}
