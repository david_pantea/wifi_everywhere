﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data.Entity.Spatial;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace WE_WebService.Helper
{
    public static class GPSCalculation
    {
        /// <summary>
        /// caluculate distance between two locations
        /// </summary>
        /// <param name="lat1"></param>
        /// <param name="lon1"></param>
        /// <param name="lat2"></param>
        /// <param name="lon2"></param>
        /// <returns></returns>
        //public static double DistanceInKmBetweenLocations(double lat1, double lon1, double lat2, double lon2)
        //{
        //    var earthRadiusKm = 6371;

        //    var dLat = DegreesToRadians(lat2 - lat1);
        //    var dLon = DegreesToRadians(lon2 - lon1);

        //    var aa = Math.Sin(dLat / 2) * Math.Sin(dLat / 2) + Math.Sin(dLon / 2) * Math.Sin(dLon / 2) * Math.Cos(DegreesToRadians(lat1)) * Math.Cos(DegreesToRadians(lat2));

        //    var cc = 2 * Math.Atan2(Math.Sqrt(aa), Math.Sqrt(1 - aa));
        //    return earthRadiusKm * cc;
        //}

        //private static double DegreesToRadians(double degrees)
        //{
        //    return degrees * Math.PI / 180;
        //}

        public static DbGeography CreatePoint(double lat, double lon, int srid = 4326)
        {
            string wkt = String.Format("POINT({0} {1})", lon, lat);

            return DbGeography.PointFromText(wkt, srid);
        }

        public static string GetCountryByCoords(double? lat, double? lon)
        {
            WebRequest request = WebRequest.Create("http://api.geonames.org/countryCodeJSON?lat=" + lat + "&lng=" + lon + "&username=davidremus");
            WebResponse response = request.GetResponse();
            Stream dataStream = response.GetResponseStream();
            StreamReader reader = new StreamReader(dataStream);
            string json = reader.ReadToEnd();
            JObject obj = JObject.Parse(json);
            try
            {
                return obj["countryName"].ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot found the country", ex);
            }
        }
    }
}