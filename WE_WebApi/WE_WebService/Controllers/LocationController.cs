﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WE_DataModel;
using WE_Mapping.Location;
using WE_Repository;
using WE_WebService.Helper;
using WE_WebService.Middleware;

namespace WE_WebService.Controllers
{
    [RoutePrefix("api/Location")]
    public class LocationController : WifiBaseController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        [HttpGet]
        [JwtAuthentication]
        [Route("")]
        public List<LocationDTO> Get()
        {
            List<location> location = unitOfWork.LocationRepository.Get().ToList();
            List<LocationDTO> locationDTO = Mapper.Map<List<LocationDTO>>(location);
            return locationDTO;
        }

        [HttpGet]
        [JwtAuthentication]
        [Route("{id}")]
        public LocationDTO Get(int id)
        {
            location location = unitOfWork.LocationRepository.Get(x => x.ID == id).FirstOrDefault();
            LocationDTO locationDTO = Mapper.Map<LocationDTO>(location);
            return locationDTO;
        }

    }
}