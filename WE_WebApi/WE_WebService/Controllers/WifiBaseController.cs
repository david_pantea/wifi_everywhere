﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WE_DataModel;
using WE_DataModel.User;
using WE_Repository;

namespace WE_WebService.Controllers
{
    public class WifiBaseController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        public user GetCurrentUser()
        {
            var userID = int.Parse(User.Identity.Name);
            user user = unitOfWork.UserRepository.Get(usr => usr.ID == userID).FirstOrDefault();

            return user;
        }

        public List<role> GetCurrentRoles()
        {
            List<role> roles = unitOfWork.RoleRepository.Get(x => x.User == GetCurrentUser()).ToList();
            return roles;
        }

        public bool isAdministrator()
        {
            var currentUser = GetCurrentUser();
            var adminRole = unitOfWork.RoleRepository.Get(x => x.Name == "Administrator").FirstOrDefault();
            if (currentUser.Roles.Contains(adminRole))
            {
                return true;
            }
            return false;
        }
    }
}
