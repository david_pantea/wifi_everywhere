﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WE_DataModel;
using WE_DataModel.User;
using WE_Repository;
using WE_WebService.Helper;
using WE_WebService.Middleware;

namespace WE_WebService.Controllers
{
    [RoutePrefix("api/Download")]
    public class DownloadController : WifiBaseController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        [JwtAuthentication]
        [HttpGet]
        [Route("")]
        public IList Get()
        {
            var locations = unitOfWork.LocationRepository.Get().ToList();
            var countries = new List<string>();
            foreach (var l in locations)
            {
                var country = GPSCalculation.GetCountryByCoords(l.Location.Latitude, l.Location.Longitude);
                if (country != null && !countries.Contains(country))
                {
                    countries.Add(country);
                }
            }
            return countries;
        }

        [JwtAuthentication]
        [HttpPost]
        [Route("Post")]
        public HttpResponseMessage Post([FromBody] dynamic country)
        {
            string c = country.country;
            string path = GenerateFileForDownload(c);

            var response = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new System.IO.FileStream(path, System.IO.FileMode.Open);
            response.Content = new StreamContent(stream);
            response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

            return response;
        }

        /// <summary>
        /// apply taxes for each new router
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        private string GenerateFileForDownload(string country)
        {
            string path = "";
            List<JObject> routersToDownload = new List<JObject>();
            foreach (var r in GetRoutersByCountry(country))
            {
                dynamic routerJSON = new JObject();
                int currentUserId = GetCurrentUser().ID;

                //retrive points if router is not unlocked
                if (unitOfWork.UnlockRepository.Get(x => x.User.ID == currentUserId && x.Router.ID == r.ID).FirstOrDefault() == null)
                {
                    //retrive points
                    user u = unitOfWork.UserRepository.Get(x => x.ID == currentUserId).FirstOrDefault();
                   
                    if (u.Points.Amount >= 1)
                    {
                        //unlock process
                        unlock newUnlock = new unlock();
                        newUnlock.Router = r;
                        newUnlock.User = unitOfWork.UserRepository.Get(x => x.ID == u.ID).FirstOrDefault();
                        newUnlock.User.Points.Amount -= 1;
                        unitOfWork.UnlockRepository.Insert(newUnlock);
                        unitOfWork.UserRepository.Update(newUnlock.User);
                        unitOfWork.Save();

                        routerJSON.ID = r.ID;
                        routerJSON.Latitude = r.Location.Location.Latitude;
                        routerJSON.Longitude = r.Location.Location.Longitude;
                        routerJSON.Name = r.Name;
                        routerJSON.Password = r.Password;
                        routersToDownload.Add(routerJSON);
                    }
                    else
                    {
                        throw new Exception("Insufficient points");                   
                    }
                }
                else
                {
                    routerJSON.ID = r.ID;
                    routerJSON.Latitude = r.Location.Location.Latitude;
                    routerJSON.Longitude = r.Location.Location.Longitude;
                    routerJSON.Name = r.Name;
                    routerJSON.Password = r.Password;
                    routersToDownload.Add(routerJSON);
                }
            }
            path = AppendToFile(country, routersToDownload);

            return path;
        }

        private IList<router> GetRoutersByCountry(string country)
        {
            List<router> routersDb = new List<router>(unitOfWork.RouterRepository.Get(x => x.IsDeleted == false).ToList());
            List<router> routers = new List<router>(routersDb);

            foreach (var r in routers)
            {
                if (GPSCalculation.GetCountryByCoords(r.Location.Location.Latitude, r.Location.Location.Longitude) != country)
                {
                    routersDb.Remove(r);
                }
            }
            return routersDb;
        }

        private string AppendToFile(string country, List<JObject> routers)
        {
            string path = System.Web.HttpContext.Current.Request.MapPath("~\\UserTempFiles\\" + GetCurrentUser().ID);
            System.IO.Directory.CreateDirectory(path);

            using (TextWriter sw = new StreamWriter(path +"\\" + country + ".data"))
            {
                sw.Write(JsonConvert.SerializeObject(routers, Formatting.Indented));
                sw.Close();
            }

            return path + "\\" + country + ".data";
        }
    }
}