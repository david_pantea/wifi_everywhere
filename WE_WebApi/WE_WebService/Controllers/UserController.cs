﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using WE_DataModel;
using WE_Repository;
using WE_WebService.Middleware;
using Newtonsoft.Json;
using WE_Mapping.User;
using AutoMapper;
using System.Security.Cryptography;
using WE_DataModel.User;

namespace WE_WebService.Controllers
{
    //[Route("api/[controller]")]
    public class UserController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();
        //[AllowAnonymous]
        //public UserDTO Get(string username, string password)
        //{
        //    user user = unitOfWork.UserRepository.Get(usr => usr.Username == username && usr.Password == password).FirstOrDefault();
        //    if (user != null)
        //    {
        //        //var obj = new { token = JwtManager.GenerateToken(user.ID.ToString())};
        //        UserDTO userDTO = Mapper.Map<UserDTO>(user);
        //        userDTO.Token = JwtManager.GenerateToken(user.ID.ToString());
        //        return userDTO;
        //    }
        //    return null;
        //}

        [HttpPost]
        [AllowAnonymous]
        public UserDTO Post([FromBody] dynamic userToLoggin)
        {
            //login the user
            string username = userToLoggin.Username;
            string passwordHash = userToLoggin.Password;
            user user = unitOfWork.UserRepository.Get(usr => usr.Username == username && usr.Password == passwordHash).FirstOrDefault();
            if (user != null)
            {
                UserDTO userDTO = Mapper.Map<UserDTO>(user);
                if (user.IsLocked == true)
                {
                    userDTO.Token = "-1";
                    return userDTO;
                }
                //var obj = new { token = JwtManager.GenerateToken(user.ID.ToString())};

                userDTO.Token = JwtManager.GenerateToken(user.ID.ToString());
                return userDTO;
            }

            return null;
        }

        [AllowAnonymous]
        [HttpPost]
        public int Post(int id, [FromBody] UserDTO userToRegister)
        {
            user newUser = Mapper.Map<user>(userToRegister);
            user who = null;
            if (newUser != null)
            {
                if (unitOfWork.UserRepository.Get(x => x.Username == newUser.Username).FirstOrDefault() != null)
                {
                    return 1;
                }
                else if (unitOfWork.UserRepository.Get(x => x.Email == newUser.Email).FirstOrDefault() != null)
                {
                    return 2;
                }
                else if (newUser.InvitationCode != null)
                {
                    who = unitOfWork.UserRepository.Get(x => x.InvitationCode == newUser.InvitationCode).FirstOrDefault();
                    if (who == null)
                        return 3;
                }

         
                newUser.InvitationCode = Get8Digits();
                //points
                point p = new point();
                p.Amount = 10;
                newUser.Points = p;


                //set user's default role
                role role = unitOfWork.RoleRepository.Get(x => x.Name == "User").FirstOrDefault();
                if (role != null)
                {
                    newUser.Roles.Add(role);
                }
                newUser.Password = userToRegister.Password;
                if (who != null)
                {
                    newUser.InvitedById = who.ID.ToString();
                    who.Points.Amount += 15;
                }
                unitOfWork.UserRepository.Insert(newUser);
                unitOfWork.Save();
                return -1;

            }

            return -2;
        }

        /// <summary>
        /// return current user
        /// </summary>
        /// <returns></returns>
        [JwtAuthentication]
        [HttpGet]
        public UserDTO Get()
        {
            //var roles = User.Identity.Cl
            //return id of auth user
            var userID = int.Parse(User.Identity.Name);
            user user = unitOfWork.UserRepository.Get(usr => usr.ID == userID).FirstOrDefault();
            UserDTO userDTO = Mapper.Map<UserDTO>(user);

            return userDTO;
        }

        //[JwtAuthentication]
        //public string Get(int token)
        //{

        //    return "loggedOut";
        //}

        [JwtAuthentication]
        [HttpPut]
        public int Put([FromBody] UserDTO userDTO)
        {
            var loggedID = int.Parse(User.Identity.Name);
            var userToUpdate = unitOfWork.UserRepository.Get(x => x.ID == loggedID).FirstOrDefault();
            if (userToUpdate.Username == userDTO.Username)
            {
                Mapper.Map(userDTO, userToUpdate);
                if (!string.IsNullOrEmpty(userDTO.Password))
                {
                    userToUpdate.Password = userDTO.Password;
                }
                unitOfWork.UserRepository.Update(userToUpdate);
                unitOfWork.Save();
                return userToUpdate.ID;
            }
            else
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }

        }

        private string Get8Digits()
        {
            var bytes = new byte[4];
            var rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            uint random = BitConverter.ToUInt32(bytes, 0) % 100000000;
            return String.Format("{0:D8}", random);
        }
    }
}