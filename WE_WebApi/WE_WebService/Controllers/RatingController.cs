﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WE_Repository;
using WE_WebService.Middleware;
using WE_DataModel;
using AutoMapper;
using WE_Mapping.Router;
using WE_Mapping;

namespace WE_WebService.Controllers
{
    public class RatingController : ApiController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        [JwtAuthentication]
        [HttpPut]
        public double Put(int id, [FromBody]int ratingValue)
        {
            router router = unitOfWork.RouterRepository.Get(r => r.IsDeleted == false && r.ID == id).FirstOrDefault();
            if (router != null)
            {
                int loggedID = int.Parse(User.Identity.Name);
                user user = unitOfWork.UserRepository.Get(usr => usr.ID == loggedID).FirstOrDefault();
                //check if we have 24 hrs difference
                var lastRating = unitOfWork.RatingRepository.Get(x => x.Router.ID == id && user.ID == x.User.ID).OrderByDescending(d => d.Time).FirstOrDefault();
                bool canRate = true;
                if (lastRating != null)
                {
                    TimeSpan diff = DateTime.Now - lastRating.Time;
                    if (diff.TotalHours < 24)
                    {
                        canRate = false;
                    }
                }
                if (canRate)
                {
                    rating r = new rating();
                    r.User = user;
                    r.Value = ratingValue;
                    r.Router = router;
                    r.Time = DateTime.Now;
                    unitOfWork.RatingRepository.Insert(r);
                    unitOfWork.Save();
                    router routerUpdated = unitOfWork.RouterRepository.Get(x => x.IsDeleted == false && x.ID == id).FirstOrDefault();
                    RouterDTO routerDTO = new RouterDTO();
                    Mapper.Map(routerUpdated, routerDTO);
                    return routerDTO.Rating;
                }
                else
                {
                    return -1;
                }
            }
            return 0;
        }

        [JwtAuthentication]
        [HttpGet]
        public List<RatingDTO> Get(int routerID, int numberOfDays = 7)
        {
           // var days = DateTime.Today.AddDays(numberOfDays);
            List<rating> ratings = unitOfWork.RatingRepository.Get(x => x.Router.ID == routerID ).ToList();
            List<RatingDTO> ratingsDTO = Mapper.Map<List<RatingDTO>>(ratings);
            return ratingsDTO;
        }
    }
}
