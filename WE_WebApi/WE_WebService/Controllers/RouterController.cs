﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WE_DataModel;
using WE_Mapping.Router;
using WE_Repository;
using WE_WebService.Middleware;
using WE_WebService.Helper;
using System.Security.Claims;
using WE_DataModel.User;

namespace WE_WebService.Controllers
{
    [RoutePrefix("api/Router")]
    public class RouterController : WifiBaseController
    {
        UnitOfWork unitOfWork = new UnitOfWork();

        [JwtAuthentication]
        [HttpGet]
        public List<RouterDTO> Get()
        {
            List<router> routerx = unitOfWork.RouterRepository.Get(r => r.IsDeleted == false).ToList();
            List<RouterDTO> routerDTO = Mapper.Map<List<RouterDTO>>(routerx);
            foreach (var router in routerDTO.Where(x => x.Password != ""))
            {
                router.Password = null;
            }
            return routerDTO;
        }

        [HttpGet]
        [JwtAuthentication]
        //[Route("api/Router{latitude:double}/{longitude:double}")]
        public List<RouterDTO> Get(double latitude, double longitude)
        {
            List<RouterDTO> routersDTO = new List<RouterDTO>();
            List<router> routers = new List<router>();

            var myLocation = GPSCalculation.CreatePoint(latitude, longitude);
            routers = unitOfWork.RouterRepository.Get(x => x.Location.Location.Distance(myLocation) < 2000 && x.IsDeleted == false).ToList();

            //check if router is already unlocked
            foreach (var router in routers)
            {
                if (!CheckIfRouterIsUnlocked(router))
                {
                    router.Password = null;
                }
            }


            Mapper.Map(routers, routersDTO);
            //foreach (var router in routersDTO.Where(x => x.Password != ""))
            //{
            //    router.Password = "";
            //}

            return routersDTO;
        }


        [JwtAuthentication]
        [HttpGet]
        //[Route("{id}")]
        public RouterDTO Get(int id, bool password = false)
        {
            router routerx = unitOfWork.RouterRepository.Get(r => r.IsDeleted == false && r.ID == id).FirstOrDefault();
            RouterDTO routerDTO = Mapper.Map<RouterDTO>(routerx);

            bool returnPassword = true;

            if (password == true)
            {
                user currentUser = GetCurrentUser();

                if (!CheckIfRouterIsUnlocked(routerx))
                {
                    if (currentUser.Points.Amount >= 5)
                    {
                        ////create new unlock
                        //unlock newUnlock = new unlock();
                        //newUnlock.Router = routerx;
                        //newUnlock.User = unitOfWork.UserRepository.Get(x => x.ID == currentUser.ID).FirstOrDefault();
                        //newUnlock.User.Points.Amount -= 5;
                        //unitOfWork.UnlockRepository.Insert(newUnlock);
                        //unitOfWork.UserRepository.Update(newUnlock.User);
                        //unitOfWork.Save();
                        UnlockRouter(routerx, currentUser);
                    }
                    else
                    {
                        returnPassword = false;
                    }
                }
            }
            else
            {
                if (CheckIfRouterIsUnlocked(routerx))
                {
                    returnPassword = true;
                }
                else
                {
                    returnPassword = false;
                }
            }

            if (!returnPassword && password == true || returnPassword == false)
            {
                routerDTO.Password = null;
            }

            return routerDTO;
        }

        private void UnlockRouter(router routerx, user currentUser, bool usePoints = true)
        {
            //create new unlock
            unlock newUnlock = new unlock();
            newUnlock.Router = routerx;
            newUnlock.User = unitOfWork.UserRepository.Get(x => x.ID == currentUser.ID).FirstOrDefault();
            if (usePoints)
                newUnlock.User.Points.Amount -= 5;
            unitOfWork.UnlockRepository.Insert(newUnlock);
            unitOfWork.UserRepository.Update(newUnlock.User);
            unitOfWork.Save();
        }

        [JwtAuthentication]
        [Authorize(Roles = "Administrator")]
        [HttpDelete]
        [Route("{id}")]
        public int Delete(int id)
        {
            router routerx = unitOfWork.RouterRepository.Get(r => r.ID == id).FirstOrDefault();
            routerx.IsDeleted = true;
            unitOfWork.RouterRepository.Update(routerx);
            unitOfWork.Save();
            return id;
        }

        [JwtAuthentication]
        [HttpPut]
        public int Put([FromBody] RouterDTO routerDTO)
        {
            router ro = unitOfWork.RouterRepository.Get(r => r.IsDeleted == false && r.ID == routerDTO.ID).FirstOrDefault();

            bool ok = false;
            if (ro != null && isAdministrator())
            {
                ok = true;
            }
            else if (ro != null && (GetCurrentUser().FirstName == routerDTO.AddedBy))
            {
                ok = true;
            }

            if (ok)
            {
                //Mapper.Map(routerDTO, r);
                //ro.Name = routerDTO.Name;
                ro.Password = routerDTO.Password;
                unitOfWork.RouterRepository.Update(ro);

                unitOfWork.Save();
                return ro.ID;
            }

            return 0;
        }

        [JwtAuthentication]
        [HttpPost]
        [Route("AddNewRouter")]
        public int Post([FromBody] RouterDTO routerDTO)
        {
            router router = new router();
            Mapper.Map(routerDTO, router);

            router.Location.Location = GPSCalculation.CreatePoint(routerDTO.Location.Latitude, routerDTO.Location.Longitude);

            router.IsDeleted = false;

            var userID = int.Parse(User.Identity.Name);
            user user = unitOfWork.UserRepository.Get(usr => usr.ID == userID).FirstOrDefault();

            router.User = user;

            unitOfWork.RouterRepository.Insert(router);

            //add points
            user.Points.Amount += 5;
            unitOfWork.UserRepository.Update(user);
            unitOfWork.Save();

            //unlock this router
            user currentUser = GetCurrentUser();
            UnlockRouter(router, currentUser,false);

            unitOfWork.Save();


            return 1;
        }

        private bool CheckIfRouterIsUnlocked(router r)
        {
            user currentUser = GetCurrentUser();
            unlock checkUnlock = unitOfWork.UnlockRepository.Get(x => x.Router.ID == r.ID && x.User.ID == currentUser.ID).FirstOrDefault();
            if (checkUnlock != null)
            {
                return true;
            }
            return false;
        }
    }
}
