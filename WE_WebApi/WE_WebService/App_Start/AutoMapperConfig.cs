﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WE_DataModel;
using WE_DataModel.User;
using WE_Mapping.Location;
using WE_Mapping.Router;
using WE_Mapping.User;

namespace WE_WebService.App_Start
{
    public static class AutoMapperConfig
    {
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperProfile>());
        }
    }

    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<role, RoleDTO>();

            CreateMap<user, UserDTO>()
                .ForMember(x => x.Password, opt => opt.Ignore())
                .ReverseMap().ForMember(x => x.Password, opt => opt.Ignore());

            CreateMap<router, RouterDTO>()
                .ForPath(dest => dest.AddedBy, opt => opt.MapFrom(src => src.User.FirstName))
                .ForPath(dest => dest.City, opt => opt.MapFrom(src => src.Location.City))
                .ForPath(dest => dest.Location, opt => opt.MapFrom(src => src.Location));

            CreateMap<RouterDTO, router>();

            CreateMap<location, LocationDTO>()
                .ForPath(dest => dest.Latitude, opt => opt.MapFrom(src => src.Location.Latitude))
                .ForPath(dest => dest.Longitude, opt => opt.MapFrom(src => src.Location.Longitude))
                .ReverseMap();

        }
    }
}
