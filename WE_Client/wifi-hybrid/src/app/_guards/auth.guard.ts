import { Injectable } from '@angular/core';
import { UserService } from '../_services/user.service';


@Injectable()
export class AuthGuard {

  private isLoggedIn = false;

  constructor() { }

  login(): void {
    this.isLoggedIn = true;
  }

  // Logout a user, destroy token and remove
  // every information related to a user
  logout(): void {
    this.isLoggedIn = false;
    localStorage.clear();
  }

  // Returns whether the user is currently authenticated
  // Could check if current token is still valid
  authenticated(): boolean {
    //verific daca token ii valid cu API
    // debugger;
    // this.userService.getCurrentUser().subscribe(result => {
    //   if (result != null) {
    //     this.login();
    //   }
    //   else {
    //     this.logout();
    //   }

    // });
    return this.isLoggedIn;
  }

}