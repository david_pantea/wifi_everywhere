import { Injectable } from '@angular/core';
import { AuthenticationService } from '../_services/index';
import { User } from '../_models/user';

@Injectable()
export class Globals {
  isLoggedIn: boolean = false;
}