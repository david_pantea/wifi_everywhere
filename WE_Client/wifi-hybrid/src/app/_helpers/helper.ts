import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';


@Injectable()
export class Helper {


    constructor(private platform: Platform) {

    }

    public checkIfIsMobile(): boolean {
        if (this.platform.is('android')) {
            return true;
        }
        return false;
    }
}