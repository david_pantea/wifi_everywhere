import { Component, Input, ViewChild } from '@angular/core';
import { Rating } from '../../_models/Rating';
import { OnChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'doughnut-chart',
  templateUrl: './doughnut-chart.component.html'
})
export class DoughnutChartComponent {
  // Doughnut
  public doughnutChartLabels: string[] = ['Sucks big time', 'Kinda bad', 'Meh', 'Pretty good', 'Rocks!'];
  public doughnutChartData: number[] = [0, 0, 0, 0, 0];
  public doughnutChartType: string = 'doughnut';
  @ViewChild(BaseChartDirective) bcd;

  computeChart(ratings: Rating[]) {
    this.doughnutChartData[0] = ratings.filter(x=>x.Value==1).length;
    this.doughnutChartData[1] = ratings.filter(x=>x.Value==2).length;
    this.doughnutChartData[2] = ratings.filter(x=>x.Value==3).length;
    this.doughnutChartData[3] = ratings.filter(x=>x.Value==4).length;
    this.doughnutChartData[4] = ratings.filter(x=>x.Value==5).length;
    this.bcd.chart.update();
  }
}
