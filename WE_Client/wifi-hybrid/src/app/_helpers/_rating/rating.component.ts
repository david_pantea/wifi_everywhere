import { Component, Input, Output, EventEmitter } from '@angular/core'
import { RatingService } from '../../_services/index'
import { ToastController } from 'ionic-angular';

@Component({
    selector: 'router-rating',
    templateUrl: 'rating.component.html',
})

export class RatingComponent {
    @Input() rating: number;
    @Input() itemId: number;
    @Output() ratingClick: EventEmitter<any> = new EventEmitter<any>();


    //private toasterService: ToasterService;
    constructor(private ratingService: RatingService, private toastCtrl: ToastController) {
        //this.toasterService = toasterService;
    }

    inpustName: string;
    ngOnInit() {
        this.inpustName = this.itemId + '_rating';
    }
    onClick(rating: number): void {
        var previousRating = this.rating;
        this.ratingService.updateRating(this.itemId, rating).subscribe(result => {
            if (result == -1) {
                this.rating = previousRating;
                // this.toasterService.pop('error', 'Oops!', 'You have already voted in the last 24h!');
                let toast = this.toastCtrl.create({
                    message: 'You have already voted in the last 24h!',
                    duration: 2000
                });
                toast.present();
            }
            else {
                this.rating = result;
                //  this.toasterService.pop('success', 'Success!', 'Your vote has been submited!');
                let toast = this.toastCtrl.create({
                    message: 'Your vote has been submited!',
                    duration: 2000
                });
                toast.present();


                this.ratingClick.emit({
                    itemId: this.itemId,
                    rating: rating
                });
            }
        });

    }
}