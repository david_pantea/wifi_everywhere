export class Location {
    ID: number;
    // Latitude: number;
    // Longitude: number;
    Location:Geography;
    City: string;
    Latitude: number;
    Longitude: number;
}

export class Geography{
    Geography:any;
    Latitude:number;
    Longitude:number;

}