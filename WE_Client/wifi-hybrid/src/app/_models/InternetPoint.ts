import { Location } from ".";

export class InternetPoint {
    ID: number;
    Name: string;
    Password: string;
    City: string;
    AddedBy: string;
    LocationID: number;
    Location: Location;
    Rating: number;
}