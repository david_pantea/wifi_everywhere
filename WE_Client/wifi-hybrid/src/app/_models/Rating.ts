export class Rating {
    ID: number  ;
    Value: number;
    RouterID: string;
    UserID: string;
    Time: Date;
}