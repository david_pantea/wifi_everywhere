import { Role } from './Role';

export class User {
    Username: string;
    FirstName: string;
    LastName: string;
    Email: string;
    Token: string;
    InvitationCode: string;
    InvitedBy: User;
    Password: string;
    Roles: Role[];
    Points: Point;
}

export class Point {
    ID: number;
    Amount: number;
}