import { Component, Output, EventEmitter, ViewChild } from "@angular/core";
import { RouterService, LocationService } from "../../_services";
import { InternetPoint, Location } from "../../_models";
import { MouseEvent } from '@agm/core';
import { ThirdPartiesForLocatingService } from "../../_services/geolocating/thirdparties.service";
import { DevicePosition } from "../../_models/_local/DevicePosition";
import { Geolocation } from '@ionic-native/geolocation';
import { NavController, LoadingController } from "ionic-angular";
import { AgmMap } from "@agm/core";
import { Globals } from "../../_helpers/globals";
import { GPSCalculation } from "../../_services/geolocating/gpscalculation.service";
import { WifiNativeService } from "../../_services/wifi.service";
import { Helper } from "../../_helpers/helper";
import { AuthGuard } from "../../_guards";


@Component({
    selector: 'router-map',
    templateUrl: './routerMap.component.html'
})

export class RouterMapComponent {
    //cluj napoca default
    latitude: number = 47.646626;
    longitude: number = 23.5793724;


    allRouters: InternetPoint[] = [];

    devicePosition: DevicePosition = new DevicePosition(this.latitude, this.longitude);

    //@ViewChild(AddRouterModal) addRouterModalComponent;

    @ViewChild(AgmMap)
    public agmMap: AgmMap;

    lastPosition: DevicePosition = new DevicePosition(this.latitude, this.longitude);

    @Output() updateRoutersTable: EventEmitter<DevicePosition> = new EventEmitter();

    constructor(private routerService: RouterService, private locationService: LocationService,
        private tp: ThirdPartiesForLocatingService, private geolocation: Geolocation,
        public navCtrl: NavController,
        private gpsCalculation: GPSCalculation,
        private wifiService: WifiNativeService,
        public loadingCtrl: LoadingController,
        private helper: Helper,
        public authGuard: AuthGuard) {
        if (this.authGuard.authenticated())
            this.geoLocation();
    }

    geoLocation() {
        var _this = this;
        this.geolocation.getCurrentPosition().then((resp) => {
            _this.devicePosition.Latitude = resp.coords.latitude;
            _this.devicePosition.Longitude = resp.coords.longitude;
            _this.agmMap.triggerResize();
        }).catch((error) => {
            if (localStorage.getItem("location") != undefined || localStorage.getItem("location") != null) {
                _this.devicePosition.Latitude = JSON.parse(localStorage.getItem("location")).Latitude;
                _this.devicePosition.Longitude = JSON.parse(localStorage.getItem("location")).Longitude;
            }
            else {
                _this.devicePosition.Latitude = 46.7712;
                _this.devicePosition.Longitude = 23.6236;
                localStorage.setItem("location", JSON.stringify(_this.devicePosition));
            }
            _this.agmMap.triggerResize();
        });
        //search for new routers
        // if (this.helper.checkIfIsMobile()) {
        //     this.searchForNewRouters();
        // }
    }

    refreshAll() {
        debugger;
        this.allRouters = [];
        this.getRoutersByLocation(this.devicePosition);
    }

    getRouters() {
        this.routerService.getRouters().subscribe(result => {
            this.allRouters = result;
        });
    }

    getRoutersByLocation(pos: DevicePosition) {
        var _this = this;
        this.routerService.getRoutersByLocation(pos).subscribe(result => {
            result.forEach(function (router) {
                if (!_this.checkIfRouterAlreadyExists(router)) {
                    _this.allRouters.push(router);
                }
            });
            _this.updateRoutersTable.emit(_this.lastPosition);
        });
    }

    centerChanged($event: any) {
        var distanceBetweenLastPosAndViewPos = this.gpsCalculation.distanceInKmBetweenLocations(this.lastPosition.Latitude, this.lastPosition.Longitude, $event.lat, $event.lng);

        if (distanceBetweenLastPosAndViewPos > 0.3) {
            this.lastPosition.Latitude = $event.lat;
            this.lastPosition.Longitude = $event.lng;
            var distanceBetweenDevicePosAndViewPos = this.gpsCalculation.distanceInKmBetweenLocations(this.devicePosition.Latitude, this.devicePosition.Longitude, this.lastPosition.Latitude, this.lastPosition.Longitude);
            if (distanceBetweenDevicePosAndViewPos > 7) {
                console.log("far awaaay");
            }
            else {
                console.log("pos este:" + this.lastPosition.Latitude);
                console.log("distanta este:" + distanceBetweenDevicePosAndViewPos);
                this.getRoutersByLocation(this.lastPosition);

                this.updateRoutersTable.emit(this.lastPosition);
            }
        }
    }


    clickOnMarker(router: InternetPoint) {

        if (router.Password != null) {
            let load = this.loadingCtrl.create({
                content: 'Please Wait....',
            });
            load.present();
            this.wifiService.connectToInternetPoint(router, function () {
                load.dismiss();
            });
        }
        else {
            alert("You must unlock the router first!");
        }
    }

    async mapClicked($event: MouseEvent) {

        if (this.helper.checkIfIsMobile()) {
            var currentSSID = await this.wifiService.getCurrentSSID();
            if (currentSSID != -1 && currentSSID != "<unknown ssid>") {
                // debugger;
                this.tp.getCityNameFor($event.coords.lat, $event.coords.lng).subscribe(res => {
                    this.navCtrl.push("add-router", {
                        Latitude: $event.coords.lat,
                        Longitude: $event.coords.lng,
                        Name: currentSSID,
                        City: res,
                        ID: -1
                    })
                })
            }
            else {
                alert("You must be connected to Wifi first");
            }
        }
        else {
            //scop didactic (adaug fara sa fiu conectat de pe windows)
            debugger;
            this.tp.getCityNameFor($event.coords.lat, $event.coords.lng).subscribe(res => {
                this.navCtrl.push("add-router", {
                    Latitude: $event.coords.lat,
                    Longitude: $event.coords.lng,
                    Name: "",
                    City: res,
                    ID: -1
                })
            })
        }

    }

    private checkIfRouterAlreadyExists(router: InternetPoint): boolean {
        let ok = false;
        this.allRouters.forEach(function (r) {
            if (r.ID == router.ID)
                ok = true;;
        });
        return ok;
    }

    private async searchForNewRouters() {

        let x = await this.wifiService.searchForRouters();
        //debugger;
        console.log("available routers:" + x[0].SSID);
    }
}