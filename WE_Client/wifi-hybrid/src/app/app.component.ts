import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { LandingPage } from '../pages/landing/landing';
import { SettingsUserPage } from '../pages/user/settings-user/settings-user';
import { UserService } from './_services';
import { User } from './_models';
import { DownloadPage } from '../pages/download/download';
import { AuthGuard } from './_guards';
import { Network } from '@ionic-native/network';
import { HomePage } from '../pages/home/home';
import { Helper } from './_helpers/helper';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any }>;

  currentUser: User = new User();

  modeOnline: boolean = true;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private network: Network, public menu: MenuController, private userService: UserService, public authGuard: AuthGuard, private helper: Helper) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });


    if ((helper.checkIfIsMobile() && this.isConnected() == true) || !helper.checkIfIsMobile()) {
      this.modeOnline = true;
      // set our app's pages
      this.pages = [
        { title: 'Home', component: "landing" },
        { title: 'User Settings', component: "settings-user" },
        { title: 'Downloads', component: "download" },
        { title: 'Logout', component: "log-out" },
        //{ title: 'My First List', component: ListPage }
      ];
    }
    else if (helper.checkIfIsMobile() && this.isConnected() == false) {
      this.modeOnline = false;
      this.pages = [
        { title: 'Home', component: "landing-offline" },
        { title: 'Logout', component: "log-out" },
        //{ title: 'My First List', component: ListPage }
      ];
    }
  }

  private isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  };

  getCurrentUserInfo() {
    this.userService.getCurrentUser().subscribe(result => {
      this.currentUser = result;
    });
  }

  openPage(page) {
    this.menu.close();
    if (page.component == "log-out") {
      this.logout();
    }
    else {
      this.nav.setRoot(page.component);
    }
  }

  logout() {
    this.authGuard.logout();
    this.nav.setRoot(HomePage);
  }
}

