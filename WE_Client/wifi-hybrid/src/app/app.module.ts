import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TypicodeInterceptor } from './typicode.interceptor';


import { Globals } from './_helpers/globals';
import { ToastController } from 'ionic-angular';

//modules

//import { ChartsModule } from 'ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//pages
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

//components
import { ModalComponent } from './_helpers/_modals/modal.component';
//import { TopMenuComponent } from './topmenu/topmenu.component';
//import { DoughnutChartComponent } from './_helpers/_chart/doughnut-chart.component';
import { RouterService, RegisterService, ThirdPartiesForLocatingService, DownloadService } from './_services/index';
import { AuthenticationService } from './_services/authentication.service';
import { AuthGuard } from './_guards';
import { DataService } from './_services/api_service/dataservice.service';
import { Configuration } from './_services/api_service/configuration.service';
import { RatingService } from './_services/rating.service';
import { UserService } from './_services/user.service';
import { LocationService } from './_services/location.service';
import { Geolocation } from '@ionic-native/geolocation';
import { SettingsUserPage } from '../pages/user/settings-user/settings-user';
import { LandingPage } from '../pages/landing/landing';
import { RouterMapComponent } from './components/routerMap/routerMap.component';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';
import { GPSCalculation } from './_services/geolocating/gpscalculation.service';
import { WifiNativeService } from './_services/wifi.service';
import { DownloadPage } from '../pages/download/download';
import { HomePageModule } from '../pages/home/home.module';
import { SettingsUserPageModule } from '../pages/user/settings-user/settings-user.module';
import { DownloadPageModule } from '../pages/download/download.module';
import { Helper } from './_helpers/helper';
import { Network } from '@ionic-native/network';


@NgModule({
  declarations: [
    MyApp,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAWnV6Zut1SpFlZ_NjkRlzt4O1bHgh490k'
    }),
    AgmJsMarkerClustererModule,
    HomePageModule,
    //LandingPageModule,
    SettingsUserPageModule,
    DownloadPageModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RouterService,
    { provide: HTTP_INTERCEPTORS, useClass: TypicodeInterceptor, multi: true },
    AuthGuard,
    AuthenticationService,
    Globals,
    DataService,
    Configuration,
    RatingService,
    UserService,
    ToastController,
    LocationService,
    ThirdPartiesForLocatingService,
    Geolocation,
    GPSCalculation,
    WifiNativeService,
    Helper,
    DownloadService,
    Network
  ]
  ,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
