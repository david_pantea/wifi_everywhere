import { Injectable } from '@angular/core';

@Injectable()
export class GPSCalculation {


    constructor() { }

    public distanceInKmBetweenLocations(lat1: number, lon1: number, lat2: number, lon2: number): number {
        var earthRadiusKm = 6371;

        var dLat = this.degreesToRadians(lat2 - lat1);
        var dLon = this.degreesToRadians(lon2 - lon1);

        var aa = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(this.degreesToRadians(lat1)) * Math.cos(this.degreesToRadians(lat2));

        var cc = 2 * Math.atan2(Math.sqrt(aa), Math.sqrt(1 - aa));
        return earthRadiusKm * cc;
    }

    private degreesToRadians(degrees: number): number {
        return degrees * Math.PI / 180;
    }
}
