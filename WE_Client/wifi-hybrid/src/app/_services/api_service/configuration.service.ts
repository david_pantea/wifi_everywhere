import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

@Injectable()
export class Configuration {
    //public Server = 'https://wifiservice.azurewebsites.net/';

    public Server: string;
    public ApiUrl = 'api';
    public ServerWithApiUrl: string;

    constructor(private platform: Platform) {
        if (this.platform.is('android')) {
            this.Server = 'https://wifiservice.azurewebsites.net/';
            //this.Server = 'http://192.168.0.100:50919/';
        }
        else {
            this.Server = 'http://localhost:50919/';
        }
        this.ServerWithApiUrl = this.Server + this.ApiUrl;
    }
}