import { Injectable } from "@angular/core";
import { DataService } from "./api_service/dataservice.service";
import { Observable } from "rxjs";

@Injectable()
export class DownloadService {

    constructor(private _dataService: DataService) {

    }

    public getCountries(): Observable<string[]> {
        return this._dataService.GetX("Download", {}).map((response: string[]) => {
            return response;
        });
    }

    public getFileForCountry(country: string): Observable<any> {
        return this._dataService.Add("Download", {country:country}, "Post").map((response: any) => {
            return response;
        });
    }
}