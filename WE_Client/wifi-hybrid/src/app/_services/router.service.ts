import { Injectable, Output } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AuthenticationService } from '../_services/authentication.service';
import { InternetPoint } from '../_models/index';
import { ToasterService } from 'angular2-toaster';
import { DataService } from './api_service/dataservice.service';
import { ToastController } from 'ionic-angular';
import { DevicePosition } from '../_models/_local/DevicePosition';

@Injectable()
export class RouterService {

    // private toasterService: ToasterService;

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService, private _dataService: DataService, private toastCtrl: ToastController) {
    }

    getRouters(): Observable<InternetPoint[]> {
        return this._dataService.GetX("Router", {}).map((response: InternetPoint[]) => {
            return response;
        });
    }

    //get near routers
    getRoutersByLocation(devicePos: DevicePosition) {
        if (devicePos != null) {
            return this._dataService.GetX("Router", { latitude: devicePos.Latitude, longitude: devicePos.Longitude }).map((response: InternetPoint[]) => {
                return response;
            });
        }
    }

    getRouterById(id: number, password = false): Observable<InternetPoint> {
        return this._dataService.GetX('Router', { id: id, password: password })
            .map((response: Response) => {
                return response;
            })
            .catch(e => {
                if (e.status === 401) {
                    //  this.toasterService.pop('error', 'Error!', 'You have no access!');
                    let toast = this.toastCtrl.create({
                        message: `You have no access!`,
                        duration: 2000
                    });
                    toast.present();
                    return Observable.throw('Unauthorized');
                }
            });
    }

    updateRouter(internetPoint: InternetPoint): Observable<number> {
        return this._dataService.Update("Router", internetPoint).map((response: number) => {
            return response;
        });
    }

    deleteRouter(internetPointId: number): Observable<number> {
        return this._dataService.Delete("Router", internetPointId).map((response: number) => {
            return response;
        });
    }

    addRouter(internetPoint: InternetPoint): Observable<number> {
        return this._dataService.Add("Router", internetPoint, "AddNewRouter").map((response: number) => {
            return response;
        });
    }

}