import { Injectable } from "@angular/core";
import { InternetPoint } from "../_models";
import { LoadingController } from "ionic-angular";
import { Observer } from "rxjs";

declare var WifiWizard: any;

@Injectable()
export class WifiNativeService {

    constructor() { }

    public async connectToInternetPoint(router: InternetPoint, callback) {
        let ok = true;
        if (!await this.checkIfWifiIsOn()) {
            if (!await this.enableWifi()) {
                ok = false;
            }

        }
        if (ok) {
            var config = WifiWizard.formatWPAConfig(router.Name, router.Password);
            WifiWizard.addNetwork(config, function () {
                WifiWizard.connectNetwork(router.Name, function () {
                   // alert("we started connecting...");

                    if (callback) {
                        callback();
                    }
                }, function () {
                    alert("fail");

                    if (callback) {
                        callback();
                    }
                });
            });
        }
    }

    async enableWifi() {
        return new Promise((resolve, reject) => {
            WifiWizard.setWifiEnabled(true, function () {
                resolve(true);
            },
                function () {
                    resolve(false);
                });
        });
    }

    async checkIfWifiIsOn() {
        return new Promise((resolve, reject) => {
            WifiWizard.isWifiEnabled(function (result) {
                if (result) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            }, function () {
                throw new Error('Something bad happened');
            });
        })
    }

    async searchForRouters() {

        return new Promise((resolve, reject) => {

            let ok = true;
            if (!this.checkIfWifiIsOn()) {
                if (!this.enableWifi()) {
                    ok = false;
                }
            }

            //  let internetPoints: any[];
            if (ok) {
                WifiWizard.startScan(function () {
                    //console.log("start scanning...");

                    WifiWizard.getScanResults(function (result) {
                       // console.log(result);
                        resolve(result);
                    }, function () {
                        console.log("Failed to get scan results.");
                    });
                }, 3000);
            }
            // return internetPoints;
        });
    }

    async getCurrentSSID() {
        return new Promise((resolve, reject) => {
            //debugger;
            WifiWizard.getCurrentSSID(function (result) {
                resolve(result);
            }, function () {
                resolve(-1);
            });
        });
    }
}