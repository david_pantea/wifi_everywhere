export * from './authentication.service';
export * from './user.service';
export * from './router.service';
export * from './rating.service';
export * from './location.service';
export * from './register.service';
export * from './geolocating/thirdparties.service';
export * from './download.service';
export * from './wifi.service';