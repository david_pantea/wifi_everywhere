import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable' // much better
import { Subscription } from 'rxjs/Subscription' // much better
import { Globals } from '../_helpers/globals';
import { DataService } from './api_service/dataservice.service';
import { User } from '../_models/index';
import { Md5 } from 'ts-md5/dist/md5';
import { AuthGuard } from '../_guards';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: Http, private globals: Globals, private _dataService: DataService, public authGuard: AuthGuard) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.Token;
        if (this.token) {
            this.globals.isLoggedIn = true;
        }
    }

    login(username: string, password: string): Observable<any> {
        return this._dataService.Add('User', { "Username": username, "Password": Md5.hashStr(password) })
            .map((response: User) => {
                // login successful if there's a jwt token in the response
                let token = response && response.Token;
                if (token) {
                    if (token == "-1")
                        return "-1";
                    this.token = token;
                    localStorage.setItem('currentUser', JSON.stringify(response));
                    this.globals.isLoggedIn = true;
                    this.authGuard.login();
                    return true;
                } else {
                    this.globals.isLoggedIn = false;
                    this.authGuard.logout();
                    return false;
                }
            });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.globals.isLoggedIn = false;
        this.authGuard.logout();
        localStorage.removeItem('currentUser');
    }
}