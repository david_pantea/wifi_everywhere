import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthenticationService } from '../../app/_services';
import { LandingPage } from '../landing/landing';
import { Network } from '@ionic-native/network';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "login"
})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  model: any = {};
  loading = false;
  error: boolean = false;
  errorMessage: string = "";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private authenticationService: AuthenticationService,
    private network: Network) {
  }

  ionViewDidLoad() {
    // reset login status
    this.authenticationService.logout();
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
    //dummy login
   // this.authenticationService.login("david", "david")
      .subscribe(result => {
        if (result === true) {
          // login successful
          this.navCtrl.setRoot("landing");
        }
        else if (result == -1) {
          this.errorMessage = "Account LOCKED!";
          this.error = true;
          this.loading = false;
        }
        else {
          // login failed
          this.error = true;
          this.errorMessage = "Wrong username or password!";
          this.loading = false;
        }
      });
  }
  goToRegister() {
    this.navCtrl.push("register");
  }

}
