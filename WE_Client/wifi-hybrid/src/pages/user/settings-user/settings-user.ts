import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { UserService } from '../../../app/_services';
import { User } from '../../../app/_models';
import { AuthGuard } from '../../../app/_guards';

@IonicPage({
  name: "settings-user"
}
)
@Component({
  selector: 'page-settings-user',
  templateUrl: 'settings-user.html',
})
export class SettingsUserPage {

  currentUser: User = new User();

  constructor(public navCtrl: NavController, public navParams: NavParams, private userService: UserService, private toastCtrl: ToastController, public authGuard: AuthGuard) {
    this.getCurrentUser();
  }

  ionViewCanEnter() {
    this.authGuard.authenticated();
  }

  updateUser() {
    this.userService.updateCurrentUserSettings(this.currentUser).subscribe(
      data => {
        let toast = this.toastCtrl.create({
          message: 'The user has been updated!',
          duration: 2000
        });
        toast.present();
        this.currentUser.Password = null;
      },
      err => {
        let toast = this.toastCtrl.create({
          message: 'The user has not been updated!',
          duration: 2000
        });
        toast.present();
      }
    );
  }

  getCurrentUser() {
    this.userService.getCurrentUser().subscribe(
      data => {
        this.currentUser = data;
        this.currentUser.Password = null;
      });
  }

}
