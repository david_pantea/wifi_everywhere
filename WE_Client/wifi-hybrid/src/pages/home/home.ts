import { Component } from '@angular/core';
import { NavController, IonicPage, ToastController } from 'ionic-angular';
import { AuthGuard } from '../../app/_guards';
import { MenuController } from 'ionic-angular';
import { Network } from '@ionic-native/network';

@IonicPage({
  name: "home",
  segment: "home"
})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public authGuard: AuthGuard, public menu: MenuController,private network: Network, private toastCtrl: ToastController) {

  }

  private isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }

  goToLogin() {
    if (this.isConnected() != null && this.isConnected() == false) {
      this.navCtrl.setRoot("landing-offline");
    }
    else{
      this.navCtrl.push("login");
    }
  
  }
  goToRegister() {
    if (this.isConnected() != null && this.isConnected() == false) {
      let toast = this.toastCtrl.create({
        message: 'You have no internet connection!',
        duration: 2000
      });
      toast.present();
    }
    else{
      this.navCtrl.push("register");
    }
  }
}
