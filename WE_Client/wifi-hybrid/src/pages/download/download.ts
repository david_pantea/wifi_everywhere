import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { DownloadService } from '../../app/_services';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { AuthGuard } from '../../app/_guards';

/**
 * Generated class for the DownloadPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "download"
})
@Component({
  selector: 'page-download',
  templateUrl: 'download.html',
})
export class DownloadPage {

  countries: Array<string> = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private downloadService: DownloadService,
    public loadingCtrl: LoadingController,
    private file: File,
    private filePath: FilePath,
    public authGuard: AuthGuard,
    private toastCtrl: ToastController) {

    let load = this.loadingCtrl.create({
      content: 'Please Wait....',
    });
    load.present();
    downloadService.getCountries().subscribe(results => {
      results = results.sort();
      this.countries = results;
      load.dismiss();
    });
  }

  ionViewDidLoad() {
    this.file.checkDir(this.file.externalDataDirectory, "localData")
      .then(x => console.log("este"))
      .catch(err => {
        this.file.createDir(this.file.externalDataDirectory, "localData", false)
          .then(x => console.log("dir created"));
      });

  }

  getFileByCountry(country: string) {
    let load = this.loadingCtrl.create({
      content: 'Downloading data....',
    });
    load.present();
    this.downloadService.getFileForCountry(country).subscribe(result => {
      this.file.createFile(this.file.externalDataDirectory + "/localData", country + ".data", true)
        .then(x => {
          console.log(result);
          this.file.writeFile(this.file.externalDataDirectory + "/localData", country + ".data", result, { replace: true });
          load.dismiss();
        });
    }, err => {
      let toast = this.toastCtrl.create({
        message: 'There were not enough points for all access points, but we created a file using all available points.',
        duration: 4000
      });
      toast.present();
    })
  }

  ionViewCanEnter() {
    this.authGuard.authenticated();
  }

}
