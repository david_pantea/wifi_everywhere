import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DownloadPage } from './download';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

@NgModule({
  declarations: [
    DownloadPage,
  ],
  imports: [
    IonicPageModule.forChild(DownloadPage),
  ],
  providers: [
    File,
    FilePath,
  ]
})
export class DownloadPageModule { }
