import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthenticationService, RegisterService } from '../../app/_services';
import { User, Point } from '../../app/_models';
import { Md5 } from 'ts-md5';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "register"
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  model: any = {};
  error: string = "";


  constructor(public navCtrl: NavController, public navParams: NavParams,
    private authenticationService: AuthenticationService,
    private registerService: RegisterService, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    this.authenticationService.logout();
  }

  register() {
    if (this.model.Password == this.model.Repassword) {
      let newUser: User = {
        Username: this.model.Username,
        FirstName: this.model.FirstName,
        LastName: this.model.LastName,
        Email: this.model.Email,
        Token: "",
        InvitationCode: this.model.InvitationCode,
        InvitedBy: null,
        Points: new Point(),
        Password: Md5.hashStr(this.model.Password).toString(),
        Roles: []
      }

      this.registerService.registerNewUser(newUser).subscribe(result => {
        if (result == -1) {
          let toast = this.toastCtrl.create({
            message: `You created a new account!`,
            duration: 2000
          });
          toast.present();
          //go to login
          this.navCtrl.setRoot("login");
        }
        else if (result == 1) {
          this.error = "This username already exists!"
        }
        else if (result == 2) {
          this.error = "This email address already exists!"
        }
        else if (result == 3) {
          this.error = "This invitation code is invalid!"
        }
        else if (result == -2) {
          //this.toasterService.pop('error', 'Oops!', 'We have some problems');
          let toast = this.toastCtrl.create({
            message: `'We have some problems!`,
            duration: 2000
          });
          toast.present();
        }
      })
    }
    else {
      this.error = "The passwords are not equal!"
    }
  }

  goToLogin() {
    this.navCtrl.pop();
  }

}
