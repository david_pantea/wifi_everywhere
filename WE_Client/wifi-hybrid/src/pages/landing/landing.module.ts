import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandingPage } from './landing';
import { RouterMapComponent } from '../../app/components/routerMap/routerMap.component';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer'

@NgModule({
  declarations: [
    LandingPage,
    RouterMapComponent
  ],
  imports: [
    IonicPageModule.forChild(LandingPage),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAWnV6Zut1SpFlZ_NjkRlzt4O1bHgh490k'
    }),
    AgmJsMarkerClustererModule,
  ],
})
export class LandingPageModule { }
