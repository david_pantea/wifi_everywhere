import { Component, ViewChild, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController, LoadingController } from 'ionic-angular';
import { RouterService, UserService, WifiNativeService } from '../../app/_services/index';
import { InternetPoint, User } from '../../app/_models';
import { AuthGuard } from './../../app/_guards/auth.guard';
import { DevicePosition } from '../../app/_models/_local/DevicePosition';
import { MyApp } from '../../app/app.component';
import { AlertController } from 'ionic-angular';
import { RouterMapComponent } from '../../app/components/routerMap/routerMap.component';

/**
 * Generated class for the RoutersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "landing",
  segment: "landing"
})
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  allRouters: InternetPoint[] = [];

  currentUser: User = new User();
  totalPoints: number = 0;

  @ViewChild(RouterMapComponent)
  public rmc: RouterMapComponent;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private routerService: RouterService,
    public authGuard: AuthGuard,
    public events: Events,
    public modalCtrl: ModalController,
    private userService: UserService,
    private alertCtrl: AlertController,
    private wifiService: WifiNativeService,
    public loadingCtrl: LoadingController, ) {
    if (this.authGuard.authenticated()) {
      this.userService.getCurrentUser().subscribe(result => {
        this.currentUser = result;
        this.getPoints();
      });
    }
  }

  listenEvents() {
    this.events.subscribe('reloadData', () => {
      this.rmc.refreshAll();
      this.getRouters(true);
      this.getPoints();
    });
  }
  ionViewDidEnter() {
    this.listenEvents();
  }
  ionViewCanEnter() {
    this.authGuard.authenticated();
  }

  ionViewDidLoad() {
    // this.navCtrl.popToRoot();
    if (!this.authGuard.authenticated()) {
      this.navCtrl.setRoot("home");
    }
    else {
      //this.getRouters();
    }
  }
  getRouters(refresh: boolean = false, $event = null) {
    let dp = $event as DevicePosition;
    let __this = this;
    if (dp != null) {
      this.routerService.getRoutersByLocation(dp).subscribe(result => {
        result.forEach(function (router) {
          if (!__this.checkIfRouterAlreadyExists(router)) {
            __this.allRouters.push(router);
          }
        });
        if (refresh) {
          this.navCtrl.setRoot(this.navCtrl.getActive().component);
        }
      });
    }
  }

  private getPoints() {
    this.userService.getCurrentUser().subscribe(result => {
      this.totalPoints = result.Points.Amount;
    });
  }

  private checkIfRouterAlreadyExists(router: InternetPoint): boolean {
    let ok = false;
    this.allRouters.forEach(function (r) {
      if (r.ID == router.ID)
        ok = true;;
    });
    return ok;
  }

  goToEdit(internetPointId: number) {
    this.navCtrl.push("edit-router", {
      id: internetPointId
    });
  }

  goToDelete(internetPointId: number) {
    this.navCtrl.push("delete-router", {
      id: internetPointId
    });
  }

  openRouterInfoModal(internetPointId: number) {
    let myModal = this.modalCtrl.create('stats-router', { id: internetPointId });
    myModal.present();
  }

  public isAdmin(): boolean {
    if (this.currentUser.Roles[0].Name == "Administrator") {
      return true;
    }
    else {
      return false;
    }
  }

  public checkIfCanEdit(internetPoint: InternetPoint): boolean {
    if (internetPoint.AddedBy == this.currentUser.FirstName) {
      return true;
    }
    return false;
  }

  getPassword(router: InternetPoint) {
    this.routerService.getRouterById(router.ID, true).subscribe(result => {
      if (result.Password == null) {
        alert("You have not enough points to unlock the router!");
      }
      else {
        this.connectToRouter(result);
      }
    })
  }


  unlockRouter(router: InternetPoint) {
    let alert = this.alertCtrl.create({
      title: 'Confirm purchase',
      message: 'Do you want to connect on this router?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            //console.log('cancel clicked');
          }
        },
        {
          text: 'Pay',
          handler: () => {
            this.getPassword(router);
          }
        }
      ]
    });
    alert.present();
  }

   connectToRouter(router: InternetPoint) {
    let load = this.loadingCtrl.create({
      content: 'Please Wait....',
    });
    load.present();
    var __this=this;
    this.wifiService.connectToInternetPoint(router, async function () {
      var currentSSID = await __this.wifiService.getCurrentSSID() as any;
      currentSSID=currentSSID+"";
      load.dismiss();
      if(currentSSID.substring(1,currentSSID.length -1)==router.Name){
        alert("connected to access point");
      }
      else{
        alert("fail to connect")
      }
    });


  }

}
