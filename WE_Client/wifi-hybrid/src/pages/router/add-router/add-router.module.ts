import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddRouterPage } from './add-router';

@NgModule({
  declarations: [
    AddRouterPage,
  ],
  imports: [
    IonicPageModule.forChild(AddRouterPage),
  ],
})
export class AddRouterPageModule {}
