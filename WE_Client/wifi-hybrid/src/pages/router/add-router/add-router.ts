import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { InternetPoint, Location, Geography } from '../../../app/_models';
import { RouterService } from '../../../app/_services';

/**
 * Generated class for the AddRouterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "add-router"
}
)
@Component({
  selector: 'page-add-router',
  templateUrl: 'add-router.html',
})
export class AddRouterPage {

  internetPoint: InternetPoint = new InternetPoint();

  manualAddRouterName: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private routerService: RouterService, private toastCtrl: ToastController, public events: Events) {
    this.internetPoint.Location = new Location();
    this.internetPoint.Location.Latitude = navParams.get('Latitude');
    this.internetPoint.Location.Longitude = navParams.get('Longitude');
    this.internetPoint.Location.City = navParams.get('City');
    this.internetPoint.Location.ID = navParams.get('ID');
    //scop didactic
    if (navParams.get('Name') != "")
      this.internetPoint.Name = navParams.get('Name').slice(1, -1);
    else {
      this.manualAddRouterName = true;
    }
  }

  addRouter() {
    this.routerService.addRouter(this.internetPoint).subscribe(result => {
      if (result != 0) {
        let toast = this.toastCtrl.create({
          message: 'The router ' + this.internetPoint.Name + ' has been added!',
          duration: 2000
        });
        toast.present();
        this.navCtrl.pop();
        this.events.publish("reloadData");
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'The router ' + this.internetPoint.Name + ' has not been added!',
          duration: 2000
        });
        toast.present();
      }
    });
  }

}
