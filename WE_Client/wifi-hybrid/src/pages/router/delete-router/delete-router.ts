import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { RouterService } from '../../../app/_services';
import { InternetPoint } from '../../../app/_models';

/**
 * Generated class for the DeleteRouterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "delete-router"
})
@Component({
  selector: 'page-delete-router',
  templateUrl: 'delete-router.html',
})
export class DeleteRouterPage {

  internetPoint: InternetPoint = new InternetPoint();

  internetPointId: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private routerService: RouterService, private toastCtrl: ToastController, private events: Events) {
    this.internetPointId = navParams.get('id');
    this.getRouter();
  }

  getRouter() {
    this.routerService.getRouterById(this.internetPointId).subscribe(result => {
      this.internetPoint = result;
    });
  }

  deleteRouter() {
    this.routerService.deleteRouter(this.internetPointId).subscribe(result => {
      if (result != 0) {
        let toast = this.toastCtrl.create({
          message: 'The router ' + this.internetPoint.Name + ' has been deleted!',
          duration: 2000
        });
        toast.present();
        this.navCtrl.pop();
        this.events.publish("reloadData");
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'The router ' + this.internetPoint.Name + ' has not been deleted!',
          duration: 2000
        });
        toast.present();
      }
    });
  }

}
