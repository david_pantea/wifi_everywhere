import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteRouterPage } from './delete-router';

@NgModule({
  declarations: [
    DeleteRouterPage,
  ],
  imports: [
    IonicPageModule.forChild(DeleteRouterPage),
  ],
})
export class DeleteRouterPageModule {}
