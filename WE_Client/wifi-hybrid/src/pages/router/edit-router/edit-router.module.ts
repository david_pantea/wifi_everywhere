import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditRouterPage } from './edit-router';

@NgModule({
  declarations: [
    EditRouterPage,
  ],
  imports: [
    IonicPageModule.forChild(EditRouterPage),
  ],
})
export class EditRouterPageModule {}
