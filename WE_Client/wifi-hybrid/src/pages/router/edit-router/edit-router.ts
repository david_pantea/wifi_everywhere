import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events } from 'ionic-angular';
import { InternetPoint } from '../../../app/_models';
import { RouterService } from '../../../app/_services';

/**
 * Generated class for the EditRouterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "edit-router"
})
@Component({
  selector: 'page-edit-router',
  templateUrl: 'edit-router.html',
})
export class EditRouterPage {

  internetPointId: number;
  internetPoint: InternetPoint = new InternetPoint();
  internetPointOldName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private routerService: RouterService, private toastCtrl: ToastController, public events: Events) {
    this.internetPointId = navParams.get('id');
  }

  ionViewDidLoad() {
    this.routerService.getRouterById(this.internetPointId).subscribe(result => {
      this.internetPoint = result;
      this.internetPointOldName = this.internetPoint.Name
    });
  }

  updateRouter() {
    this.routerService.updateRouter(this.internetPoint).subscribe(result => {
      if (result != 0) {
        let toast = this.toastCtrl.create({
          message: 'The router ' + this.internetPoint.Name + ' has been updated!',
          duration: 2000
        });
        toast.present();
        this.navCtrl.pop();
        this.events.publish("reloadData");
      }
      else {
        let toast = this.toastCtrl.create({
          message: 'The router ' + this.internetPoint.Name + ' has not been updated!',
          duration: 2000
        });
        toast.present();
      }
    }, (err) => {
      if (err === 'Unauthorized') {
      }
    });
  }

}
