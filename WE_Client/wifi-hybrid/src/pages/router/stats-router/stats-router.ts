import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { Rating } from '../../../app/_models';
import { RatingService, RouterService } from '../../../app/_services';
import { DoughnutChartComponent } from '../../../app/_helpers/_chart/doughnut-chart.component';

/**
 * Generated class for the StatsRouterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'stats-router'
})
@Component({
  selector: 'page-stats-router',
  templateUrl: 'stats-router.html',
})
export class StatsRouterPage {

  internetPointId: number;

  ratings: Rating[];

  rating: number;

  canVote = false;

  @ViewChild(DoughnutChartComponent) doughnut;

  constructor(private ratingService: RatingService, params: NavParams, public viewCtrl: ViewController, private routerService: RouterService) {
    this.internetPointId = params.get("id");
    this.getStarRating();
    this.getRatingChart();
  }

  closeModal() {
    this.viewCtrl.dismiss();
  }

  getStarRating() {
    this.routerService.getRouterById(this.internetPointId).subscribe(result2 => {
      //debugger;
      this.rating = result2.Rating;
      if (result2.Password != null) {
        this.canVote = true;
      }
    })
  }

  getRatingChart() {
    this.ratingService.getRating(this.internetPointId, 14).subscribe(result => {
      this.ratings = result;
      // debugger;
      this.doughnut.computeChart(this.ratings);
    });
  }
}
