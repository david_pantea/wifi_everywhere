import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StatsRouterPage } from './stats-router';
import { ChartsModule } from 'ng2-charts';
import { DoughnutChartComponent } from '../../../app/_helpers/_chart/doughnut-chart.component';
import { RatingComponent } from '../../../app/_helpers/_rating/rating.component';

@NgModule({
  declarations: [
    StatsRouterPage,
    DoughnutChartComponent,
    RatingComponent
  ],
  imports: [
    IonicPageModule.forChild(StatsRouterPage),
    ChartsModule,
  ],
  entryComponents:[StatsRouterPage]
})
export class StatsRouterPageModule {}
