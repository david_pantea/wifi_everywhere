import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LandingOfflinePage } from './landing-offline';

@NgModule({
  declarations: [
    LandingOfflinePage,
  ],
  imports: [
    IonicPageModule.forChild(LandingOfflinePage),
  ],
})
export class LandingOfflinePageModule {}
