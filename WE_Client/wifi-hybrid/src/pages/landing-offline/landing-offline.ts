import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ThirdPartiesForLocatingService, WifiNativeService } from '../../app/_services';
import { GPSCalculation } from '../../app/_services/geolocating/gpscalculation.service';
import { InternetPoint } from '../../app/_models';
import { Geolocation } from '@ionic-native/geolocation';
import { DevicePosition } from '../../app/_models/_local/DevicePosition';

/**
 * Generated class for the LandingOfflinePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "landing-offline",
  segment: "landing-offline"
})
@Component({
  selector: 'page-landing-offline',
  templateUrl: 'landing-offline.html',
})
export class LandingOfflinePage {
  //cluj napoca default
  latitude: number = 47.646626;
  longitude: number = 23.5793724;
  devicePosition: DevicePosition = new DevicePosition(this.latitude, this.longitude);

  internetPoints: InternetPoint[] = [];
  countries: string[] = [];
  currentWifiList: any[] = [];
  isHereList: boolean[] = [];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private file: File,
    private tp: ThirdPartiesForLocatingService,
    private gpsCalculation: GPSCalculation,
    private geolocation: Geolocation,
    private wifiService: WifiNativeService,
    public loadingCtrl: LoadingController) {
    //this.geolocate();
  }


  geolocate() {
    var _this = this;
    this.geolocation.getCurrentPosition().then((resp) => {
      _this.devicePosition.Latitude = resp.coords.latitude;
      _this.devicePosition.Longitude = resp.coords.longitude;
      //console.log(resp.coords.latitude);
    });
  }


  ionViewDidLoad() {
    this.getCurrentAPS();
    this.readDataFromLocalFile();
  }

  getCurrentAPS() {
    this.currentWifiList = [];
    var _this = this;
    var currentWifiList = this.wifiService.searchForRouters();
    currentWifiList.then(function (value: any) {
      value.forEach(element => {
        _this.currentWifiList.push(element);
      });
    })
  }

  readDataFromLocalFile() {
    this.file.checkDir(this.file.externalDataDirectory, "localData")
      .then(x => this.file.listDir(this.file.externalDataDirectory, "localData")
        .then(y => y.forEach(f => {
          this.tp.readLocalFile(f.name).subscribe(res => {
            var currentIndex = 0;
            res.forEach(ip => {
              this.internetPoints.push(ip);
              this.countries.push(f.name.substring(0, f.name.length - 5));
              debugger;
              this.currentWifiList.forEach(cwl => {
                if (ip.Name == cwl.SSID) {
                  this.isHereList[currentIndex] = true;
                }
              });
              currentIndex++;
            })
          });
        })))
  }

  connectToRouter(router: InternetPoint) {
    let load = this.loadingCtrl.create({
      content: 'Please Wait....',
    });
    load.present();
    this.wifiService.connectToInternetPoint(router, function () {
      load.dismiss();
    });

  }
}
