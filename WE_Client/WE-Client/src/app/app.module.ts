import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//auth
import { AuthGuard } from './_guards/index';
import { AuthenticationService, UserService, RouterService, RatingService, RegisterService } from './_services/index';
import { DataService } from './_services/api_service/dataservice.service';

//modules
import { RouterModule } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { ChartsModule } from 'ng2-charts';
import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

import { Globals } from './_helpers/globals';

import { HttpModule } from '@angular/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TypicodeInterceptor } from './typicode.interceptor';

//services
import { LocationService } from './_services/location.service';
import { Configuration } from './_services/api_service/configuration.service';
import { ThirdPartiesForLocatingService } from './_services/geolocating/thirdparties.service';

//components
import { AppComponent } from './app.component';
import { ModalComponent } from './_helpers/_modals/modal.component';
import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';
import { SideBarComponent } from './sidebar/sidebar.component';
import { RouterComponent } from './router/router.component';
import { RatingComponent } from './_helpers/_rating/rating.component';
import { ViewRatingStatsModal } from './router/viewRatingStatsModal.component';
import { DoughnutChartComponent } from './_helpers/_chart/doughnut-chart.component';
import { LocateRouterModal } from './router/locateRouterModal/locateRouterModal.component';
import { RegisterComponent } from './register/register.component';
import { RouterMapComponent } from './router/routerMap/routerMap.component';
import { AddRouterModal } from './router/addRouterModal/addRouterModal.component';
import { DeleteRouterModal } from './router/deleteRouterModal/deleteRouterModal.component';
import { EditRouterModal } from './router/editRouterModal/editRouterModal.component';
import { SettingsComponent } from './settings/settings.component';
import { TopMenuComponent } from './topmenu/topmenu.component';
import { ConnectToWifiComponent } from './router/connectToWifi/connect-to-wifi.component';

@NgModule({
  imports: [
    BrowserModule,
    BsDropdownModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToasterModule,
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAWnV6Zut1SpFlZ_NjkRlzt4O1bHgh490k'
    }),
    AgmJsMarkerClustererModule,
    RouterModule.forRoot([
      { path: 'login', component: LoginComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'landing', component: LandingComponent, canActivate: [AuthGuard] },
      { path: 'routers', component: RouterComponent, canActivate: [AuthGuard] },
      { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },

      { path: '', redirectTo: 'landing', pathMatch: 'full' },
      { path: '**', redirectTo: 'landing', pathMatch: 'full' }
    ])
  ],

  declarations: [AppComponent, LandingComponent, LoginComponent, SideBarComponent, RouterComponent, ModalComponent, 
    EditRouterModal, DeleteRouterModal, AddRouterModal, RatingComponent, ViewRatingStatsModal, DoughnutChartComponent, 
    LocateRouterModal, RegisterComponent, RouterMapComponent, SettingsComponent,TopMenuComponent, ConnectToWifiComponent],

  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TypicodeInterceptor,
    multi: true
  },
    AuthGuard,
    AuthenticationService,
    UserService, Globals, RouterService, LocationService, RatingService, ToasterService, 
    DataService, Configuration, RegisterService, ThirdPartiesForLocatingService],

  bootstrap: [AppComponent]
})
export class AppModule { }
