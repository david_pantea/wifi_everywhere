import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from '../_services/index';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    error: boolean = false;
    errorMessage :string="";

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }

    login() {
        this.loading = true;
        this.authenticationService.login(this.model.username, this.model.password)
            .subscribe(result => {
                if (result === true) {
                    // login successful
                    this.router.navigate(['/']);
                } 
                else if(result == -1){
                    this.errorMessage = "Account LOCKED!";
                    this.error = true;
                    this.loading = false;
                }
                else {
                    // login failed
                    this.error = true;
                    this.errorMessage = "Wrong username or password!";
                    this.loading = false;
                }
            });
    }
}