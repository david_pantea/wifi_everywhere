import { Component } from '@angular/core';
import { UserService } from '../_services/index';
import { User } from '../_models/user';
import { ToasterService } from 'angular2-toaster';

@Component({
    moduleId: module.id,
    templateUrl: 'settings.component.html',
    styleUrls: ['settings.component.css']
})

export class SettingsComponent {

    currentUser: User = new User();
    private toasterService: ToasterService;

    constructor(private userService: UserService, toasterService: ToasterService) {
        this.getCurrentUser();
        this.toasterService = toasterService;
    }

    updateUser() {
        this.userService.updateCurrentUserSettings(this.currentUser).subscribe(
            data => {
                this.toasterService.pop('success', 'Success!', 'The user has been updated!');
                this.currentUser.Password=null;
            },
            err => {
                this.toasterService.pop('error', 'Error!', 'The user has not been updated!');
            }
        );
    }

    getCurrentUser() {
        this.userService.getCurrentUser().subscribe(
            data => {
                this.currentUser = data;
            });
    }
}