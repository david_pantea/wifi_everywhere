import { Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from '../_services/authentication.service';
import { InternetPoint, Rating } from '../_models/index';
import { DataService } from './api_service/dataservice.service';

@Injectable()
export class RatingService {


    constructor(
        private authenticationService: AuthenticationService,
        private _dataService: DataService) {
    }

    updateRating(routerID: number, newRatingValue: number): Observable<number> {
        return this._dataService.UpdateX('Rating', routerID, newRatingValue)
            .map((response: number) => {
                return response
            });
    }

    getRating(routerID: number, numberOfDays: number): Observable<Rating[]> {
        return this._dataService.GetX('Rating', { 'routerID': routerID, 'numberOfDays': numberOfDays })
            .map((response: Rating[]) => {
                return response
            });
    }

}