"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var globals_1 = require("../_helpers/globals");
var AuthenticationService = (function () {
    function AuthenticationService(http, globals) {
        this.http = http;
        this.globals = globals;
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.Token;
        if (this.token) {
            this.globals.isLoggedIn = true;
        }
    }
    AuthenticationService.prototype.login = function (username, password) {
        // let headers = new Headers();
        // //headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjEiLCJuYmYiOjE1MTUwMDQxMzcsImV4cCI6MTUxNTAwNTMzNywiaWF0IjoxNTE1MDA0MTM3fQ.T4hlLdRa7iR2UA2xWmM3eaK7b-yrcvEQ5KACKqkg4vA');
        // headers.append('access_token', 'db3OIsj+BXE9NZDy0t8W3TcOMNhy+2d/1sFnWG4HnV8TZY30iTOdtVWMUh8GWvB1GlOgJuQZdcF2Luqm/hccMw==');
        var _this = this;
        // let options = new RequestOptions({ headers: headers });
        return this.http.get('http://localhost:50919/api/User/Login?username=' + username + '&password=' + password)
            .map(function (response) {
            // login successful if there's a jwt token in the response
            var token = response.json() && JSON.parse(response.json()).Token;
            if (token) {
                // set token property
                _this.token = token;
                // store username and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', response.json());
                _this.globals.isLoggedIn = true;
                // return true to indicate successful login
                return true;
            }
            else {
                // return false to indicate failed login
                _this.globals.isLoggedIn = false;
                return false;
            }
        });
    };
    AuthenticationService.prototype.logout = function () {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.globals.isLoggedIn = false;
        localStorage.removeItem('currentUser');
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, globals_1.Globals])
], AuthenticationService);
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map