import { TestBed, inject } from '@angular/core/testing';

import { ThirdpartiesService } from './thirdparties.service';

describe('ThirdpartiesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ThirdpartiesService]
    });
  });

  it('should be created', inject([ThirdpartiesService], (service: ThirdpartiesService) => {
    expect(service).toBeTruthy();
  }));
});
