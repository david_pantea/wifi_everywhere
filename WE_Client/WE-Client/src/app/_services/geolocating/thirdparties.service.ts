import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ThirdPartiesForLocatingService {

  private apiKeyGoogleMaps = "AIzaSyDAC-lElv_PILtXgOUTV_P8j4-uvhZpp4I";

  private httpOptions = {
    headers: new HttpHeaders({
      'externalApi': 'externalApi'
    })
  };

  constructor(private http: HttpClient) { }


  public getCityNameFor(lat: number, lng: number): Observable<string> {
    return this.http.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + "," + lng + "&key=" + this.apiKeyGoogleMaps, this.httpOptions)
      .map(result => {
        return result["results"][0].address_components[2].long_name;
      });

  }
}
