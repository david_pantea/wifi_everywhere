import 'rxjs/add/operator/map';

import { HttpClient, HttpHeaders, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Configuration } from '../api_service/configuration.service';




@Injectable()
export class DataService {

    private actionUrl: string;

    constructor(private http: HttpClient, private _configuration: Configuration) {
        this.actionUrl = _configuration.ServerWithApiUrl;
    }


    public Get<T>(controllerName: string, id: number): Observable<T> {
        return this.http.get<T>(this.actionUrl + "/" + controllerName + "/" + id);
    }

    public GetX<T>(controllerName: string, params: any): Observable<T> {
        let myParams = this.ExplodeParams(params);
        return this.http.get<T>(this.actionUrl + "/" + controllerName, { params: myParams });
    }

    public Add<T>(controllerName: string, item: any): Observable<T> {
        return this.http.post<T>(this.actionUrl + "/" + controllerName, item);
    }
    public AddX<T>(controllerName: string, item: any, id: number): Observable<T> {
        return this.http.post<T>(this.actionUrl + "/" + controllerName + "/" + id, item);
    }

    public Update<T>(controllerName: string, itemToUpdate: any): Observable<T> {
        return this.http.put<T>(this.actionUrl + "/" + controllerName, itemToUpdate);
    }

    public UpdateX<T>(controllerName: string, id: number, params: any): Observable<T> {
        var body = JSON.stringify(params);
        debugger;
        return this.http.put<T>(this.actionUrl + "/" + controllerName + "/" + id, body);
    }

    public Delete<T>(controllerName: string, id: number): Observable<T> {
        return this.http.delete<T>(this.actionUrl + "/" + controllerName + "/" + id);
    }

    private ExplodeParams(params: any): HttpParams {
        let myParams = new HttpParams();
        for (var propertyName in params) {
            myParams = myParams.append(propertyName, params[propertyName]);
        }
        return myParams;
    }
}


