import { Injectable } from '@angular/core';

@Injectable()
export class Configuration {
    public Server = 'https://wifiservice.azurewebsites.net/';
   // public Server = 'http://localhost:52280/';
    public ApiUrl = 'api';
    public ServerWithApiUrl = this.Server + this.ApiUrl;
}