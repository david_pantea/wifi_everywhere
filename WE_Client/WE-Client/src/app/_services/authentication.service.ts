import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable' // much better
import { Subscription } from 'rxjs/Subscription' // much better
import { Globals } from '../_helpers/globals';
import { DataService } from './api_service/dataservice.service';
import { User } from '../_models/index';
import { retry } from 'rxjs/operators/retry';
import { Md5 } from 'ts-md5/dist/md5';

@Injectable()
export class AuthenticationService {
    public token: string;

    constructor(private http: Http, private globals: Globals, private _dataService: DataService) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.Token;
        if (this.token) {
            this.globals.isLoggedIn = true;
        }
    }

    login(username: string, password: string): Observable<any> {
        //  let headers = new Headers();
        // //headers.append('Authorization', 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1bmlxdWVfbmFtZSI6IjEiLCJuYmYiOjE1MTUwMDQxMzcsImV4cCI6MTUxNTAwNTMzNywiaWF0IjoxNTE1MDA0MTM3fQ.T4hlLdRa7iR2UA2xWmM3eaK7b-yrcvEQ5KACKqkg4vA');
        //  headers.append('access_token', 'db3OIsj+BXE9NZDy0t8W3TcOMNhy+2d/1sFnWG4HnV8TZY30iTOdtVWMUh8GWvB1GlOgJuQZdcF2Luqm/hccMw==');

        //  let options = new RequestOptions({ headers: headers });
        // let userToLogginX = { 'username': username, 'password': password };

        return this._dataService.Add('User', { "Username": username, "Password": Md5.hashStr(password) })
            .map((response: User) => {
                // login successful if there's a jwt token in the response
                let token = response && response.Token;
                if (token) {
                    if (token == "-1")
                        return "-1";
                    this.token = token;
                    localStorage.setItem('currentUser', JSON.stringify(response));
                    this.globals.isLoggedIn = true;
                    return true;
                } else {
                    this.globals.isLoggedIn = false;
                    return false;
                }
            });
        // // //FIXIT
        // return this._dataService.GetX<User>("User", { username: username, password: password }).map((response: User) => {
        //     // login successful if there's a jwt token in the response
        //     let token = response && response.Token;
        //     if (token) {
        //         this.token = token;
        //         localStorage.setItem('currentUser', JSON.stringify(response));
        //         this.globals.isLoggedIn = true;
        //         return true;
        //     } else {
        //         this.globals.isLoggedIn = false;
        //         return false;
        //     }
        // });
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        this.globals.isLoggedIn = false;
        localStorage.removeItem('currentUser');
    }
}