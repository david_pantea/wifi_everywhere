import { Injectable, Output } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';


import { AuthenticationService } from '../_services/authentication.service';
import { InternetPoint } from '../_models/index';
import { ToasterService } from 'angular2-toaster';
import { error } from 'selenium-webdriver';
import { DataService } from './api_service/dataservice.service';

@Injectable()
export class RouterService {

    private toasterService: ToasterService;

    constructor(
        private http: Http,
        private authenticationService: AuthenticationService, toasterService: ToasterService, private _dataService: DataService) {
        this.toasterService = toasterService;
    }

    getRouters(): Observable<InternetPoint[]> {
        return this._dataService.GetX("Router",{}).map((response: InternetPoint[]) => {
            return response;
        });
    }

    getRouterById(id: number): Observable<InternetPoint> {
        return this._dataService.Get('Router', id)
            .map((response: Response) => {
                return response;
            })
            .catch(e => {
                if (e.status === 401) {
                    this.toasterService.pop('error', 'Error!', 'You have no access!');
                    return Observable.throw('Unauthorized');
                }
            });
    }

    updateRouter(internetPoint: InternetPoint): Observable<number> {
        return this._dataService.Update("Router", internetPoint).map((response: number) => {
            return response;
        });
    }

    deleteRouter(internetPointId: number): Observable<number> {
        return this._dataService.Delete("Router", internetPointId).map((response: number) => {
            return response;
        });
    }

    addRouter(internetPoint: InternetPoint): Observable<number> {
        return this._dataService.Add("Router", internetPoint).map((response: number) => {
            return response;
        });
    }

}