import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import { Subscription } from 'rxjs/Subscription';

import { AuthenticationService } from '../_services/authentication.service';
import { User } from '../_models/index';
import { DataService } from './api_service/dataservice.service';
import { Md5 } from 'ts-md5';

@Injectable()
export class UserService {
    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
        private dataService: DataService) {
    }



    getUsers(): Observable<User[]> {
        // add authorization header with jwt token
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.authenticationService.token });
        let options = new RequestOptions({ headers: headers });

        // get users from api
        return this.http.get('/api/users', options)
            .map((response: Response) => response.json());
    }

    getCurrentUser(): Observable<User> {
        //return JSON.parse(localStorage.getItem('currentUser'));
        return this.dataService.Get("User", null).map((response: User) => {
            return response;
        });
    }

    updateCurrentUserSettings(userToUpdate: User): Observable<number> {
        if (userToUpdate.Password != null) {
            userToUpdate.Password = Md5.hashStr(userToUpdate.Password) as string;
        }

        return this.dataService.Update("User", userToUpdate).map((response: number) => {
            return response;
        });
    }
}