import { Injectable, Output } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
 
import { AuthenticationService } from '../_services/authentication.service';
import { Location } from '../_models/index';
import { DataService } from './api_service/dataservice.service';
 
@Injectable()
export class LocationService {


    constructor(
        private http: Http,
        private authenticationService: AuthenticationService,
        private _dataService: DataService) {
    }

    getLocations(): Observable<Location[]> {
        return this._dataService.GetX('Location',{})
            .map((response: Location[]) => {
                return response;
            });
    }

    getLocationById(locationID: number): Observable<Location> {
        return this._dataService.Get('Location',locationID)
            .map((response: Location) => {
                return response;
            });
    }
}