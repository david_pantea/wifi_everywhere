import { Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { AuthenticationService } from '../_services/authentication.service';
import { DataService } from './api_service/dataservice.service';
import { User } from '../_models';

@Injectable()
export class RegisterService {

    constructor(
        private authenticationService: AuthenticationService, private _dataService:DataService) {         
    }

    public registerNewUser(newUser:User): Observable<any>{
        return this._dataService.AddX('User', newUser,0)
        .map((response: Response) => {
            return response;
        });
    }
}