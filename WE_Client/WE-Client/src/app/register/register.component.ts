import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, RegisterService } from '../_services';
import { User } from '../_models';
import { ToasterService } from 'angular2-toaster';
import { Md5 } from 'ts-md5';


@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html'
})

export class RegisterComponent implements OnInit {

    model: any = {};
    error: string = "";
    private toasterService: ToasterService;

    constructor(private router: Router, private authenticationService: AuthenticationService, private registerService: RegisterService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        // reset login status
        this.authenticationService.logout();
    }

    register() {
        if (this.model.Password == this.model.Repassword) {
            let newUser: User = {
                Username: this.model.Username,
                FirstName: this.model.FirstName,
                LastName: this.model.LastName,
                Email: this.model.Email,
                Token: "",
                InvitationCode: this.model.InvitationCode,
                InvitedBy: null,
                Password:Md5.hashStr(this.model.Password).toString()
            }

            this.registerService.registerNewUser(newUser).subscribe(result => {
                if (result == -1){
                    this.toasterService.pop('success', 'Success!', 'You created a new account!');
                    this.router.navigateByUrl('/login')
                }
                else if (result==1){
                    this.error="This username already exists!"
                }
                else if (result==2){
                    this.error="This email address already exists!"
                }
                else if (result==3){
                    this.error="This invitation code is invalid!"
                }
                else if (result==-2){
                    this.toasterService.pop('error', 'Oops!', 'We have some problems');
                }
            })
        }
        else {
            this.error = "The passwords are not equal!"
        }
    }

}