export class User {
    Username: string;
    FirstName: string;
    LastName: string;
    Email: string;
    Token: string;
    InvitationCode: string;
    InvitedBy: User;
    Password: string;
}