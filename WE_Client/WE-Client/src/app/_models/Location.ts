export class Location {
    ID: number;
    Latitude: number;
    Longitude: number;
    City: string;
}