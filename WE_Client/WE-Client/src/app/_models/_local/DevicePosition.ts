export class DevicePosition {
    Latitude: number;
    Longitude: number;

    /**
     *
     */
    constructor(latitude: number, longitude: number) {
        this.Latitude = latitude;
        this.Longitude = longitude;
    }
}