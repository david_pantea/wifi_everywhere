import { Component, Input, EventEmitter, Output, ViewChild } from "@angular/core";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { RatingService } from "../_services/index";
import { InternetPoint, Rating } from "../_models/index";
import { ToasterService } from "angular2-toaster";
import { DoughnutChartComponent } from "../_helpers/_chart/doughnut-chart.component";

@Component({
  selector: 'rating-stats-modal',
  template: `
  <i class="glyphicon glyphicon-stats" (click)="modal.show(); getRatingChart()" ></i>
  <app-modal #modal>
    <div class="app-modal-header" >
      View Rating Stats for NUME ROUTER
    </div>
    <div class="app-modal-body">
     <doughnut-chart></doughnut-chart>
    </div>
    <div class="app-modal-footer">
      <button type="button" class="btn btn-default" (click)="modal.hide()">Close</button>
    </div>
  </app-modal>
  `
})
export class ViewRatingStatsModal {
  @Input() internetPointId: number;
  @Output() refreshEvent = new EventEmitter();

  ratings: Rating[];
  
  @ViewChild(DoughnutChartComponent) doughnut;

  constructor(private ratingService: RatingService) {
  }

  getRatingChart() {
    this.ratingService.getRating(this.internetPointId, 7).subscribe(result => {
      this.ratings = result;
      debugger;
      this.doughnut.computeChart(this.ratings);
    });
  }

}