import { Component, Input, EventEmitter, Output } from "@angular/core";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { RouterService } from "../../_services/index";
import { InternetPoint } from "../../_models/index";
import { ToasterService } from "angular2-toaster";


@Component({
  selector: 'edit-router-modal',
  template: `
  <i class="glyphicon glyphicon-edit" (click)="modal.show(); getRouter()"></i>
  <app-modal #modal>
    <div class="app-modal-header" *ngIf="internetPoint">
      Edit {{internetPoint?.Name}}
    </div>
    <div class="app-modal-body">
      <div class="form-group" *ngIf="internetPoint">
        <label>Name:</label>
        <input type="text" class="form-control" id="name" value={{internetPoint?.Name}} [(ngModel)]=internetPoint.Name>
      </div>
      <div class="form-group" *ngIf="internetPoint">
      <label>Password:</label>
      <input type="text" class="form-control" id="name" value={{internetPoint?.Password}} [(ngModel)]=internetPoint.Password>
    </div>
    </div>
    <div class="app-modal-footer">
      <button type="button" class="btn btn-default" (click)="modal.hide()">Close</button>
      <button type="button" class="btn btn-primary" (click)="updateRouter()" (click)="modal.hide()">Save changes</button>
    </div>
  </app-modal>
  `
})
export class EditRouterModal {
  @Input() internetPointId: number;
  @Output() refreshEvent = new EventEmitter();

  internetPoint: InternetPoint;
  private toasterService: ToasterService;

  constructor(private routerService: RouterService, toasterService: ToasterService) {
    this.toasterService = toasterService;
  }

  getRouter() {
    this.routerService.getRouterById(this.internetPointId).subscribe(result => {
      this.internetPoint = result;
    });
  }

  updateRouter() {
    this.routerService.updateRouter(this.internetPoint).subscribe(result => {
      if (result != 0) {
        this.refreshEvent.emit();
        this.toasterService.pop('success', 'Success!', 'The router ' + this.internetPoint.Name + ' has been updated!');
      }
      else {
        this.toasterService.pop('error', 'Error!', 'The router ' + this.internetPoint.Name + ' has not been updated!');
      }
    }, (err) => {
      if (err === 'Unauthorized') {
      }
    });
  }

}