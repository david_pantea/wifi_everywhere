"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var EditRouterModal = (function () {
    //output event 
    function EditRouterModal(routerService) {
        this.routerService = routerService;
        this.refreshEvent = new core_1.EventEmitter();
    }
    EditRouterModal.prototype.ngOnInit = function () {
        var _this = this;
        this.routerService.getRoutersById(this.internetPointId).subscribe(function (result) {
            _this.internetPoint = result;
        });
    };
    EditRouterModal.prototype.updateRouter = function () {
        var _this = this;
        this.routerService.updateRouter(this.internetPoint).subscribe(function (result) {
            if (result != 0) {
                _this.refreshEvent.emit();
            }
        });
    };
    return EditRouterModal;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], EditRouterModal.prototype, "internetPointId", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], EditRouterModal.prototype, "refreshEvent", void 0);
EditRouterModal = __decorate([
    core_1.Component({
        selector: 'edit-router-modal',
        template: "\n  <i class=\"glyphicon glyphicon-edit\" (click)=\"modal.show()\"></i>\n  <app-modal #modal>\n    <div class=\"app-modal-header\" *ngIf=\"internetPoint\">\n      Edit {{internetPoint?.Name}}\n    </div>\n    <div class=\"app-modal-body\">\n      <div class=\"form-group\" *ngIf=\"internetPoint\">\n        <label>Name:</label>\n        <input type=\"text\" class=\"form-control\" id=\"name\" value={{internetPoint?.Name}} [(ngModel)]=internetPoint.Name>\n      </div>\n      <div class=\"form-group\" *ngIf=\"internetPoint\">\n      <label>Password:</label>\n      <input type=\"text\" class=\"form-control\" id=\"name\" value={{internetPoint?.Password}} [(ngModel)]=internetPoint.Password>\n    </div>\n    </div>\n    <div class=\"app-modal-footer\">\n      <button type=\"button\" class=\"btn btn-default\" (click)=\"modal.hide()\">Close</button>\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"updateRouter()\" (click)=\"modal.hide()\">Save changes</button>\n    </div>\n  </app-modal>\n  "
    }),
    __metadata("design:paramtypes", [index_1.RouterService])
], EditRouterModal);
exports.EditRouterModal = EditRouterModal;
//# sourceMappingURL=editRouterModal.component.js.map