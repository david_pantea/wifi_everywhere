import { Component, Input, EventEmitter, Output } from "@angular/core";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { RouterService } from "../../_services/index";
import { InternetPoint } from "../../_models/index";
import { ToasterService } from "angular2-toaster";


@Component({
    selector: 'delete-router-modal',
    template: `
  <i class="glyphicon glyphicon-remove" (click)="modal.show(); getRouter()"></i>
  <app-modal #modal>
    <div class="app-modal-header" *ngIf="internetPoint">
      Delete {{internetPoint?.Name}}?
    </div>
    <div class="app-modal-footer">
      <button type="button" class="btn btn-default" (click)="modal.hide()">Close</button>
      <button type="button" class="btn btn-danger" (click)="deleteRouter()" (click)="modal.hide()">Delete</button>
    </div>
  </app-modal>
  `
})
export class DeleteRouterModal {
    @Input() internetPointId: number;
    @Output() refreshEvent = new EventEmitter();

    internetPoint: InternetPoint;
    private toasterService: ToasterService;
    constructor(private routerService: RouterService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }

    getRouter() {
        this.routerService.getRouterById(this.internetPointId).subscribe(result => {
            this.internetPoint = result;
        });
    }

    deleteRouter() {
        this.routerService.deleteRouter(this.internetPointId).subscribe(result => {
            if (result != 0) {
                this.refreshEvent.emit();
                this.toasterService.pop('success', 'Success!', 'The router ' + this.internetPoint.Name + ' has been deleted!');
            }
            else {
                this.toasterService.pop('error', 'Error!', 'The router ' + this.internetPoint.Name + ' has not been deleted!');
            }
        });
    }

}