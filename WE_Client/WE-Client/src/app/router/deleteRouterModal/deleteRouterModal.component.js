"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var DeleteRouterModal = (function () {
    //output event 
    function DeleteRouterModal(routerService) {
        this.routerService = routerService;
        this.refreshEvent = new core_1.EventEmitter();
    }
    DeleteRouterModal.prototype.ngOnInit = function () {
        var _this = this;
        this.routerService.getRoutersById(this.internetPointId).subscribe(function (result) {
            _this.internetPoint = result;
        });
    };
    DeleteRouterModal.prototype.deleteRouter = function () {
        var _this = this;
        this.routerService.deleteRouter(this.internetPointId).subscribe(function (result) {
            if (result != 0) {
                _this.refreshEvent.emit();
            }
        });
    };
    return DeleteRouterModal;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], DeleteRouterModal.prototype, "internetPointId", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DeleteRouterModal.prototype, "refreshEvent", void 0);
DeleteRouterModal = __decorate([
    core_1.Component({
        selector: 'delete-router-modal',
        template: "\n  <i class=\"glyphicon glyphicon-remove\" (click)=\"modal.show()\"></i>\n  <app-modal #modal>\n    <div class=\"app-modal-header\" *ngIf=\"internetPoint\">\n      Delete {{internetPoint?.Name}}?\n    </div>\n    <div class=\"app-modal-footer\">\n      <button type=\"button\" class=\"btn btn-default\" (click)=\"modal.hide()\">Close</button>\n      <button type=\"button\" class=\"btn btn-danger\" (click)=\"deleteRouter()\" (click)=\"modal.hide()\">Delete</button>\n    </div>\n  </app-modal>\n  "
    }),
    __metadata("design:paramtypes", [index_1.RouterService])
], DeleteRouterModal);
exports.DeleteRouterModal = DeleteRouterModal;
//# sourceMappingURL=deleteRouterModal.component.js.map