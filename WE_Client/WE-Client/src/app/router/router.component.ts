import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, RouterService, UserService } from '../_services/index';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { InternetPoint } from '../_models/index';
import { RatingService } from '../_services/rating.service';

@Component({
    templateUrl: `./router.component.html`
})
export class RouterComponent implements OnInit {
    allRouters: InternetPoint[] = [];
    constructor(
        private routerService: RouterService, private ratingService:RatingService, userService: UserService) {

    }

    filteredRouters: InternetPoint[];
    private _listFilter: string;
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(v: string) {
        this._listFilter = v;
        this.filteredRouters = this.listFilter ? this.performFilter(this.listFilter) : this.allRouters;
    }

    performFilter(filterBy: string): InternetPoint[] {
        filterBy = filterBy.toLocaleLowerCase();
        return this.allRouters.filter((product: InternetPoint)=>product.Name.toLocaleLowerCase().indexOf(filterBy)!== -1);
    }

    ngOnInit() {
        this.getRouters();
    }

    getRouters(){
        this.routerService.getRouters().subscribe(result => {
            this.allRouters = result;
        });
    }
}
