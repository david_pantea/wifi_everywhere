import { Component, Input, EventEmitter, Output, ViewChild } from "@angular/core";
import { RouterService, LocationService } from "../../_services/index";
import { InternetPoint } from "../../_models/index";
import { ToasterService } from "angular2-toaster";
import { AgmMap } from "@agm/core";

@Component({
  selector: 'locate-router-modal',
  templateUrl: './locateRouterModal.component.html',
  styleUrls: ['./locateRouterModal.component.css']
})
export class LocateRouterModal {
  @Input() internetPointId: number;
  @Output() refreshEvent = new EventEmitter();

  internetPoint: InternetPoint;

  @ViewChild(AgmMap)
  public agmMap: AgmMap;
  latitude: number = 47.646626;
  longitude: number = 23.5793724;

  constructor(private routerService: RouterService, private locationService: LocationService) {

  }

  getRouter() {
    this.routerService.getRouterById(this.internetPointId).subscribe(result => {
      this.internetPoint = result;
      this.locationService.getLocationById(this.internetPoint.LocationID).subscribe(result => {
        //debugger;
        this.latitude = Number(result.Latitude);
        this.longitude = Number(result.Longitude);
      });
      this.agmMap.triggerResize();
    });
  }


}