"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var RouterComponent = (function () {
    function RouterComponent(routerService) {
        this.routerService = routerService;
        this.allRouters = [];
    }
    Object.defineProperty(RouterComponent.prototype, "listFilter", {
        get: function () {
            return this._listFilter;
        },
        set: function (v) {
            this._listFilter = v;
            this.filteredRouters = this.listFilter ? this.performFilter(this.listFilter) : this.allRouters;
        },
        enumerable: true,
        configurable: true
    });
    RouterComponent.prototype.performFilter = function (filterBy) {
        filterBy = filterBy.toLocaleLowerCase();
        return this.allRouters.filter(function (product) { return product.Name.toLocaleLowerCase().indexOf(filterBy) !== -1; });
    };
    RouterComponent.prototype.ngOnInit = function () {
        this.getRouters();
    };
    RouterComponent.prototype.getRouters = function () {
        var _this = this;
        this.routerService.getRouters().subscribe(function (result) {
            _this.allRouters = result;
        });
    };
    return RouterComponent;
}());
RouterComponent = __decorate([
    core_1.Component({
        templateUrl: "./router.component.html"
    }),
    __metadata("design:paramtypes", [index_1.RouterService])
], RouterComponent);
exports.RouterComponent = RouterComponent;
//# sourceMappingURL=router.component.js.map