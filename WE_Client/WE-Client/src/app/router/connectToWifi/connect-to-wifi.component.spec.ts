import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectToWifiComponent } from './connect-to-wifi.component';

describe('ConnectToWifiComponent', () => {
  let component: ConnectToWifiComponent;
  let fixture: ComponentFixture<ConnectToWifiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectToWifiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectToWifiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
