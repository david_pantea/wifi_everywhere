import { Component, Input, EventEmitter, Output, ViewChild } from "@angular/core";
import { OnInit } from "@angular/core/src/metadata/lifecycle_hooks";
import { RouterService, ThirdPartiesForLocatingService } from "../../_services/index";
import { InternetPoint, Location } from "../../_models/index";
import { LocationService } from "../../_services/location.service";
import { ToasterService } from "angular2-toaster";
import { AgmMap } from "@agm/core";
import { DevicePosition } from "../../_models/_local/DevicePosition";
import { MouseEvent } from '@agm/core';
import { ModalComponent } from "../../_helpers/_modals/modal.component";


@Component({
    selector: 'add-router-modal',
    templateUrl: './addRouterModal.component.html',
    styleUrls: ['./addRouterModal.component.css']
})
export class AddRouterModal {

    devicePosition: DevicePosition = new DevicePosition(46.7712, 23.6236);

    selectedLocation: number = null;

    internetPoint: InternetPoint = new InternetPoint();
    allLocations: Location[] = new Array<Location>();
    private toasterService: ToasterService;


    @ViewChild(AgmMap)
    public agmMap: AgmMap;
    //cluj napoca default

    @ViewChild('modal') modal: ModalComponent
    

    @Output() refreshEvent = new EventEmitter();
    constructor(private routerService: RouterService, private locationService: LocationService, toasterService: ToasterService,private tp: ThirdPartiesForLocatingService) {
        this.toasterService = toasterService;
        
    }

    localize(){
        if (navigator.geolocation) {
            var _this = this;
            navigator.geolocation.getCurrentPosition((position) => {
                //debugger;
                _this.devicePosition.Latitude = position.coords.latitude;
                _this.devicePosition.Longitude = position.coords.longitude;
                _this.agmMap.triggerResize();
            }, (error) => {
                // alert('code: ' + error.code + '\n' +
                //     'message: ' + error.message + '\n');
              //  debugger;
                if (localStorage.getItem("location") != undefined || localStorage.getItem("location") != null) {
                    _this.devicePosition.Latitude = JSON.parse(localStorage.getItem("location")).Latitude;
                    _this.devicePosition.Longitude = JSON.parse(localStorage.getItem("location")).Longitude;
                }
                else {
                    _this.devicePosition.Latitude = 46.7712;
                    _this.devicePosition.Longitude = 23.6236;
                    localStorage.setItem("location", JSON.stringify(_this.devicePosition));
                }
                _this.agmMap.triggerResize();
            }, { timeout: 1000 });
        }
    }

    addRouter() {
        this.internetPoint.Location = this.allLocations.pop();
        debugger;
        this.routerService.addRouter(this.internetPoint).subscribe(result => {
            if (result != 0) {
                this.refreshEvent.emit();
                this.toasterService.pop('success', 'Success!', 'The router ' + this.internetPoint.Name + ' has been added!');
                
            }
            else {
                this.toasterService.pop('error', 'Error!', 'The router ' + this.internetPoint.Name + ' has not been added!');
            }

            this.clearAllLocations();
        });
    }

    clearAllLocations(){
        this.allLocations=[];
    }

    show($event:MouseEvent){
        this.localize();
        this.modal.show();
        this.markerToMap($event);
    }

    markerToMap($event:MouseEvent){
        this.tp.getCityNameFor($event.coords.lat, $event.coords.lng).subscribe(res => {
            debugger;
                this.allLocations.push({
                    Latitude: $event.coords.lat,
                    Longitude: $event.coords.lng,
                    City: res,
                    ID: -1
                });
            });
    }


}