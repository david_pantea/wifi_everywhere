"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var index_1 = require("../_services/index");
var index_2 = require("../_models/index");
var location_service_1 = require("../_services/location.service");
var AddRouterModal = (function () {
    function AddRouterModal(routerService, locationService) {
        this.routerService = routerService;
        this.locationService = locationService;
        this.selectedLocation = null;
        this.internetPoint = new index_2.InternetPoint();
        //output event 
        this.refreshEvent = new core_1.EventEmitter();
        this.getLocations();
    }
    AddRouterModal.prototype.getLocations = function () {
        var _this = this;
        this.locationService.getLocations().subscribe(function (result) {
            _this.locations = result;
        });
    };
    AddRouterModal.prototype.addRouter = function () {
        var _this = this;
        this.internetPoint.LocationID = this.selectedLocation;
        this.routerService.addRouter(this.internetPoint).subscribe(function (result) {
            if (result != 0) {
                _this.refreshEvent.emit();
            }
        });
    };
    return AddRouterModal;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], AddRouterModal.prototype, "internetPointId", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AddRouterModal.prototype, "refreshEvent", void 0);
AddRouterModal = __decorate([
    core_1.Component({
        selector: 'add-router-modal',
        template: "\n  <a (click)=\"modal.show()\"><i class=\"glyphicon glyphicon-plus\"></i>Add new router</a>\n \n  <app-modal #modal>\n    <div class=\"app-modal-header\">\n      Add new router\n    </div>\n    <div class=\"app-modal-body\">\n      <div class=\"form-group\">\n        <label>Name:</label>\n        <input type=\"text\" class=\"form-control\" id=\"name\" placeholder=\"Name\"  [(ngModel)]=\"internetPoint.Name\">\n      </div>\n      <div class=\"form-group\">\n        <label>Password:</label>\n        <input type=\"text\" class=\"form-control\" id=\"password\" placeholder=\"Password\" [(ngModel)]=\"internetPoint.Password\">\n      </div>\n       <div class=\"form-group\">\n                <select [(ngModel)]=\"selectedLocation\">\n            <option *ngFor='let location of locations' [ngValue]=\"location.ID\">{{location.City}}</option>\n            </select>\n        </div>\n    </div>\n    <div class=\"app-modal-footer\">\n      <button type=\"button\" class=\"btn btn-default\" (click)=\"modal.hide()\">Close</button>\n      <button type=\"button\" class=\"btn btn-primary\" (click)=\"addRouter()\" (click)=\"modal.hide()\">Add</button>\n    </div>\n  </app-modal>\n  "
    }),
    __metadata("design:paramtypes", [index_1.RouterService, location_service_1.LocationService])
], AddRouterModal);
exports.AddRouterModal = AddRouterModal;
//# sourceMappingURL=addRouterModal.component.js.map