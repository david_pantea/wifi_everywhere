import { Component ,Output, EventEmitter, ViewChild} from "@angular/core";
import { RouterService, LocationService } from "../../_services";
import { InternetPoint, Location } from "../../_models";
import { MouseEvent } from '@agm/core';
import { ThirdPartiesForLocatingService } from "../../_services/geolocating/thirdparties.service";
import { DevicePosition } from "../../_models/_local/DevicePosition";
import { AddRouterModal } from "../addRouterModal/addRouterModal.component";

@Component({
    selector: 'router-map',
    templateUrl: './routerMap.component.html',
    styleUrls: ['./routerMap.component.css']
})

export class RouterMapComponent {
    latitude: number = 47.646626;
    longitude: number = 23.5793724;


    allRouters: InternetPoint[] = [];

    //cluj napoca default
    devicePosition: DevicePosition = new DevicePosition(46.7712, 23.6236);

    @ViewChild(AddRouterModal) addRouterModalComponent;
    

    constructor(private routerService: RouterService, private locationService: LocationService, private tp: ThirdPartiesForLocatingService) {
        this.getRouters();
        this.geoLocation();
    }

    geoLocation() {
        if (navigator.geolocation) {
            var _this = this;
            navigator.geolocation.getCurrentPosition((position) => {
                //debugger;
                _this.devicePosition.Latitude = position.coords.latitude;
                _this.devicePosition.Longitude = position.coords.longitude;
 
            }, (error) => {
                // alert('code: ' + error.code + '\n' +
                //     'message: ' + error.message + '\n');
              //  debugger;
                if (localStorage.getItem("location") != undefined || localStorage.getItem("location") != null) {
                    _this.devicePosition.Latitude = JSON.parse(localStorage.getItem("location")).Latitude;
                    _this.devicePosition.Longitude = JSON.parse(localStorage.getItem("location")).Longitude;
                }
                else {
                    _this.devicePosition.Latitude = 46.7712;
                    _this.devicePosition.Longitude = 23.6236;
                    localStorage.setItem("location", JSON.stringify(_this.devicePosition));
                }
        
            }, { timeout: 1000 });
        }
    }

    getRouters() {
        this.routerService.getRouters().subscribe(result => {
            this.allRouters = result;
        });
    }

    clickOnMarker(){
        alert("marker clicked");
    }

    mapClicked($event: MouseEvent) {

        this.addRouterModalComponent.show($event);
        
        // this.tp.getCityNameFor($event.coords.lat, $event.coords.lng).subscribe(res => {
        //     this.allLocations.push({
        //         Latitude: $event.coords.lat,
        //         Longitude: $event.coords.lng,
        //         City: res,
        //         ID: -1
        //     });

        // });

    }

}
