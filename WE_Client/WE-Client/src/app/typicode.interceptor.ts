import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs/observable';
@Injectable()
export class TypicodeInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    //external call
    if (req.headers.has('externalApi')) {
      req = req.clone({ headers: req.headers.delete('externalApi') });
      return next.handle(req);
    }


    if (!req.headers.has('Content-Type')) {
      req = req.clone({ headers: req.headers.set('Content-Type', 'application/json') });
    }

    var currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.Token) {
      req = req.clone({ headers: req.headers.set('Authorization', 'Bearer ' + currentUser.Token) });
    };

    req = req.clone({ headers: req.headers.set('Accept', 'application/json') });
    //console.log(JSON.stringify(req.headers));
    return next.handle(req);
  }
}