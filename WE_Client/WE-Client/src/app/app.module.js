"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
//auth
var index_1 = require("./_guards/index");
var index_2 = require("./_services/index");
var router_1 = require("@angular/router");
var angular_star_rating_1 = require("angular-star-rating");
var globals_1 = require("./_helpers/globals");
var http_1 = require("@angular/http");
var http_2 = require("@angular/common/http");
var typicode_interceptor_1 = require("./typicode.interceptor");
var app_component_1 = require("./app.component");
var modal_component_1 = require("./_helpers/_modals/modal.component");
var landing_component_1 = require("./landing/landing.component");
var login_component_1 = require("./login/login.component");
var sidebar_component_1 = require("./sidebar/sidebar.component");
var router_component_1 = require("./router/router.component");
var editRouterModal_component_1 = require("./router/editRouterModal.component");
var deleteRouterModal_component_1 = require("./router/deleteRouterModal.component");
var addRouterModal_component_1 = require("./router/addRouterModal.component");
var location_service_1 = require("./_services/location.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            forms_1.FormsModule,
            forms_1.ReactiveFormsModule,
            http_1.HttpModule,
            angular_star_rating_1.StarRatingModule,
            router_1.RouterModule.forRoot([
                { path: 'login', component: login_component_1.LoginComponent },
                { path: 'landing', component: landing_component_1.LandingComponent, canActivate: [index_1.AuthGuard] },
                { path: 'routers', component: router_component_1.RouterComponent, canActivate: [index_1.AuthGuard] },
                { path: '', redirectTo: 'landing', pathMatch: 'full' },
                { path: '**', redirectTo: 'landing', pathMatch: 'full' }
            ])
        ],
        declarations: [app_component_1.AppComponent, landing_component_1.LandingComponent, login_component_1.LoginComponent, sidebar_component_1.SideBarComponent, router_component_1.RouterComponent, modal_component_1.ModalComponent, editRouterModal_component_1.EditRouterModal, deleteRouterModal_component_1.DeleteRouterModal,
            addRouterModal_component_1.AddRouterModal],
        providers: [{
                provide: http_2.HTTP_INTERCEPTORS,
                useClass: typicode_interceptor_1.TypicodeInterceptor,
                multi: true
            },
            index_1.AuthGuard,
            index_2.AuthenticationService,
            index_2.UserService, globals_1.Globals, index_2.RouterService, location_service_1.LocationService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map