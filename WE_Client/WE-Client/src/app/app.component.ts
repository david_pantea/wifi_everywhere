import { Component } from '@angular/core';
import { AuthenticationService } from './_services/index';
import { Globals} from './_helpers/globals'

@Component({
  selector: 'my-app',
  templateUrl: `./app.component.html`,
})
export class AppComponent {
  name = 'Wifi-Everywhere';

  constructor(private authenticationService: AuthenticationService, private globals: Globals) {
  }

}
